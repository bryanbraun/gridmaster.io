require 'test_helper'

class LessonTest < ActiveSupport::TestCase
  test "challenge_data should be saved as JSON" do
    lesson = Lesson.new
    lesson.challenge_data = '{"hello": "goodbye"}'
    lesson.send(:convert_challenge_data_to_json)
    assert_equal(lesson.challenge_data, {"hello" => "goodbye"})
  end
end
