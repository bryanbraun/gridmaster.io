require 'test_helper'

class TipTest < ActiveSupport::TestCase

  def setup
    @tip1 = tips(:tip_one)
    @tip2 = tips(:tip_two)
    @tip3 = tips(:tip_three)
  end

  test "paginate to the next tip with 'next'" do
    assert_equal @tip1.next.id, @tip2.id
  end

  test "paginate to the previous tip with 'previous'" do
    assert_equal @tip2.previous.id, @tip1.id
  end

  test "paginating before the first tip, or after the last tip, returns nil" do
    assert_nil @tip1.previous
    assert_nil @tip3.next
  end

  test "published tips should get a published date" do
    current_datetime = DateTime.now

    DateTime.stub :now, current_datetime do
      new_tip = Tip.new
      new_tip.published = true
      new_tip.save

      assert_equal new_tip.published_at, current_datetime
    end
  end

  test "unpublished tips should not get a published date" do
    unpublished_new_tip = Tip.new # published is false by default
    unpublished_new_tip.save

    previously_published_existing_tip = @tip2
    previously_published_existing_tip.published = false
    previously_published_existing_tip.save

    assert_nil unpublished_new_tip.published_at
    assert_nil previously_published_existing_tip.published_at
  end

  test "updated tips should not get an updated published date" do
    original_publish_date = @tip2.published_at

    @tip2.description = "New example description"
    @tip2.save

    assert_equal original_publish_date, @tip2.published_at
  end
end
