require 'test_helper'

class CompletionServiceTest < ActionDispatch::IntegrationTest
  test "Bypass completion if challenge was already completed" do
    params_for_the_completion_already_in_fixture = {
      'user_id': challenge_completions(:bryan_step_1).user_id,
      'course_id': challenge_completions(:bryan_step_1).course_id,
      'lesson_id': challenge_completions(:bryan_step_1).lesson_id,
      'challenge_step': challenge_completions(:bryan_step_1).challenge_step,
    }

    updated_timestamp_before = challenge_completions(:bryan_step_1).updated_at
    challenge_count_before = ChallengeCompletion.count
    response = CompletionService.new.save_progress(params_for_the_completion_already_in_fixture)
    challenge_count_after = ChallengeCompletion.count
    updated_timestamp_after = ChallengeCompletion.find_by(params_for_the_completion_already_in_fixture).updated_at

    assert_equal false, response[:updated]
    assert_equal challenge_count_before, challenge_count_after
    assert_not_equal updated_timestamp_before, updated_timestamp_after
  end

  test "Add challenge completion if no completion exists" do
    params_for_completion_not_in_fixture = {
      'user_id': users(:andrew).id,
      'course_id': '1',
      'lesson_id': '1',
      'challenge_step': '1'
    }

    challenge_count_before = ChallengeCompletion.count
    response = CompletionService.new.save_progress(params_for_completion_not_in_fixture)
    challenge_count_after = ChallengeCompletion.count

    assert_equal true, response[:updated]
    assert_equal 1, response[:updated_step_number]
    assert_equal challenge_count_before + 1, challenge_count_after
  end

  test "Add lesson completion when the last unfinished challenge step in a lesson is completed" do
    params_for_the_last_unfinished_challenge = {
      'user_id': challenge_completions(:bryan_step_1).user_id,
      'course_id': challenge_completions(:bryan_step_1).course_id,
      'lesson_id': challenge_completions(:bryan_step_1).lesson_id,
      'challenge_step': '2',
    }

    lesson_count_before = LessonCompletion.count
    response = CompletionService.new.save_progress(params_for_the_last_unfinished_challenge)
    lesson_count_after = LessonCompletion.count

    assert_equal true, response[:updated]
    assert_equal 1, lesson_count_before
    assert_equal 2, lesson_count_after
  end

  test "Add course completion when the last unfinished lesson in a course is completed" do
    params_for_the_last_unfinished_challenge_in_the_last_unfinished_lesson = {
      'user_id': challenge_completions(:holly_step_3).user_id,
      'course_id': challenge_completions(:holly_step_3).course_id,
      'lesson_id': challenge_completions(:holly_step_3).lesson_id,
      'challenge_step': '2',
    }

    course_count_before = CourseCompletion.count
    response = CompletionService.new.save_progress(params_for_the_last_unfinished_challenge_in_the_last_unfinished_lesson)
    course_count_after = CourseCompletion.count

    assert_equal true, response[:updated]
    assert_equal 0, course_count_before
    assert_equal 1, course_count_after
  end

end
