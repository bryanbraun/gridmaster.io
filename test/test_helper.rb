ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/reporters'
require 'minitest/mock'

Minitest::Reporters.use!

# Stuff we put in ActiveSupport::TestCase becomes available in any classes inheriting from it,
# as well as classes with ActiveSupport::TestCase in its ancestors list, like:
#   - ActionView::TestCase
#   - ActionController::TestCase
#   - ActionDispatch::IntegrationTest
#
# Note: ActionController::TestCase is being deprecated:
#       https://github.com/rails/rails/issues/22496
class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
end

class ActionDispatch::IntegrationTest
  # @todo: I tried adding the sign_in and sign_out helpers once but they didn't work.
  #        It was taking too much time to debug so for now I am posting to the endpoints
  #        manually. There's a little overhead there, so I could try to debug this at
  #        some point in the future.
  # include Devise::Test::IntegrationHelpers

end
