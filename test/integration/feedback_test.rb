require "test_helper"

class FeedbackTest < ActionDispatch::IntegrationTest

  test "submitting feedback redirects you back to the page you were previously on" do
    post addin_user_session_path, params: {
      addin_user: {
        email: users(:bryan).email,
        password: "fixture-password"
      }
    }

    get addin_course_lesson_path(1, 1)

    post addin_feedback_create_path, params: {
      feedback: {
        email: users(:bryan).email,
        message: "example message"
      }
    }

    assert_redirected_to addin_course_lesson_path(1, 1)
  end
end
