require "test_helper"

class AddinUsersTest < ActionDispatch::IntegrationTest
  def setup
    @free_user = users(:bryan)
    @premium_user = users(:andrew)
    @premium_beta_user = users(:steven)
  end

  test "signing in returns you back to the page you were previously on" do
    get addin_course_lesson_path(1, 1)
    get new_addin_user_session_path
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }

    assert_equal I18n.t("devise.sessions.signed_in"), flash[:notice]
    assert_redirected_to addin_course_lesson_path(1, 1)
  end

  test "signing up returns you back to the page you were previously on" do
    get addin_course_lesson_path(1, 1)
    get new_addin_user_registration_path
    post addin_user_registration_path, params: {
      addin_user: {
        email: "example@user.com",
        password: "fixture-password",
        password_confirmation: "fixture-password"
      }
    }

    assert_equal I18n.t("devise.registrations.signed_up"), flash[:notice]
    assert_redirected_to addin_course_lesson_path(1, 1)
  end

  test "logging out returns you to the courses page" do
    delete destroy_addin_user_session_path

    assert_equal I18n.t("devise.sessions.signed_out"), flash[:notice]
    assert_redirected_to addin_courses_path
  end

  test "deleting your account returns you to the courses page" do
    # I tried to use `sign_in @free_user` instead of POSTing the login but
    # it just wasn"t working. POSTing worked and I needed to move forward.
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }

    delete addin_user_registration_path

    assert_equal I18n.t("devise.registrations.destroyed"), flash[:notice]
    assert_redirected_to addin_courses_path
  end

  test "the signup/login prompt is present on challenge pages for anonymous users" do
    get challenge_addin_course_lesson_path(1, 1)
    assert_select ".modal #signup"
  end

  test "the signup/login prompt is not present on challenge pages for authenticated users" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }

    get challenge_addin_course_lesson_path(1, 1)
    assert_select ".modal #signup", false
  end

  test "logging in with completion data creates a completion and redirects correctly" do
    completion_count_before = ChallengeCompletion.all.size

    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      },
      completion: {
        user_id: @premium_user.id,
        course_id: "1",
        lesson_id: "1",
        challenge_step: "1"
      }
    }

    completion_count_after = ChallengeCompletion.all.size

    assert_equal completion_count_before + 1, completion_count_after
    assert_redirected_to challenge_addin_course_lesson_path(1, 1).concat("#2")
  end

  test "signing up with completion data creates a completion and redirects correctly" do
    completion_count_before = ChallengeCompletion.all.size

    post addin_user_registration_path, params: {
      addin_user: {
        email: "example@user.com",
        password: "fixture-password",
        password_confirmation: "fixture-password"
      },
      completion: {
        user_id: "",
        course_id: "1",
        lesson_id: "1",
        challenge_step: "1"
      }
    }

    completion_count_after = ChallengeCompletion.all.size

    assert_equal completion_count_before + 1, completion_count_after
    assert_redirected_to challenge_addin_course_lesson_path(1, 1).concat("#2")
  end

  test "a failed login with completion data populates that data in the resubmit form" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "incorrect-password"
      },
      completion: {
        user_id: @premium_user.id,
        course_id: "1",
        lesson_id: "1",
        challenge_step: "1"
      }
    }

    assert_template :new

    assert_select "#completion_user_id[value='#{@premium_user.id}']"
    assert_select "#completion_course_id[value='1']"
    assert_select "#completion_lesson_id[value='1']"
    assert_select "#completion_challenge_step[value='1']"
  end

  test "a failed signup with completion data populates that data in the resubmit form" do
    post addin_user_registration_path, params: {
      addin_user: {
        email: "example@user.com",
        password: "incorrect-password",
        password_confirmation: "double-incorrect-password"
      },
      completion: {
        user_id: "",
        course_id: "1",
        lesson_id: "1",
        challenge_step: "1"
      }
    }

    assert_template :new

    assert_select "#completion_user_id[value='']"
    assert_select "#completion_course_id[value='1']"
    assert_select "#completion_lesson_id[value='1']"
    assert_select "#completion_challenge_step[value='1']"
  end

  test "upcoming courses cannot be accessed by premium users" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      }
    }

    get addin_courses_path
    assert_select ".is-coming", true
    # this test could be more accurate by attempting a click, instead of just checking for a class
  end

  test "upcoming courses can be accessed by premium_beta users" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_beta_user.email,
        password: "fixture-password"
      }
    }

    get addin_courses_path
    assert_select ".is-coming", false
    # this test could be more accurate by attempting a click, instead of just checking for a class
  end

  test "basic info can be updated independently" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }

    put addin_users_update_info_path, params: {
      addin_user: {
        name: "Bryan New Name",
        email: @free_user.email
      }
    }
    @free_user.reload

    assert_equal I18n.t("devise.registrations.update_info"), flash[:success]
    assert_equal "Bryan New Name", @free_user.name
  end

  test "password can be updated independently" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }
    put addin_users_update_password_path, params: {
      addin_user: {
        current_password: "fixture-password",
        password: "new-fixture-password",
        password_confirmation: "new-fixture-password"
      }
    }
    @free_user.reload

    assert_equal I18n.t("devise.registrations.update_password"), flash[:success]
    assert !@free_user.valid_password?("fixture-password")
    assert @free_user.valid_password?("new-fixture-password")
  end

  test "password update failures display error messages" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }
    put addin_users_update_password_path, params: {
      addin_user: {
        current_password: "incorrect password",
        password: "short",
        password_confirmation: "not matching"
      }
    }

    assert_select ".devise-validation strong", { text: /3 errors/ }
  end
end
