require "test_helper"

class AddinCompletionsTest < ActionDispatch::IntegrationTest
  def setup
    @premium_user = users(:andrew)
  end

  test "Anonymous completion responds with 401 Unauthorized (prompting the signup form)" do
    post addin_completions_create_path, xhr: true, params: {
      completion: {
        user_id: nil,
        course_id: "1",
        lesson_id: "1",
        challenge_step: "1"
      }
    }

    assert_response 401
    assert_response :unauthorized
  end

  test "Successful completion returns the data that advances you to the next challenge step" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      }
    }

    post addin_completions_create_path, xhr: true, params: {
      completion: {
        user_id: @premium_user.id,
        course_id: '1',
        lesson_id: '1',
        challenge_step: '1'
      }
    }

    response_string = response.body
    response_hash = JSON.parse(response_string)

    assert_response :success
    assert_equal true, response_hash["updated"]
    assert_equal 1, response_hash["updated_step_number"]
  end

  test "Completed challenges are indicated with checkmarks" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      }
    }

    # We're going to add completions manually for these tests because 1) it helps make these tests
    # more self-contained, and 2) we're concerned with testing checkmarks display, not completion
    # adding (which is being tested in services/completion_service.rb).
    cc = ChallengeCompletion.new(user_id: @premium_user.id, course_id: 1, lesson_id: 1, challenge_step: 1)
    cc.save

    get challenge_addin_course_lesson_path(1, 1)
    assert_select ".challenge-nav-step:nth-of-type(1).is-completed"
  end

  test "Completed lessons are indicated with checkmarks" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      }
    }

    cc = LessonCompletion.new(user_id: @premium_user.id, course_id: 1, lesson_id: 1)
    cc.save

    get addin_course_lessons_path(1)
    assert_select ".lesson-box:nth-of-type(1) .lesson-box__completed-icon"
  end

  test "Completed courses are indicated with checkmarks" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      }
    }

    cc = CourseCompletion.new(user_id: @premium_user.id, course_id: 1)
    cc.save

    get addin_courses_path
    assert_select ".course-box:nth-of-type(1) .course-box__completed-icon"
  end

end
