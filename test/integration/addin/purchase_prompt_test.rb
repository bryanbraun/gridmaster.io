require "test_helper"

class PurchasePromptTest < ActionDispatch::IntegrationTest
  def setup
    @free_user = users(:bryan)
    @premium_user = users(:andrew)
    @premium_beta_user = users(:steven)
  end

  test "the purchase prompt is present on premium lessons for free users" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }

    get addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt"
    get challenge_addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt"
  end

  test "the purchase prompt is not present on premium lessons for premium users" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_user.email,
        password: "fixture-password"
      }
    }

    get addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
    get challenge_addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
  end

  test "the purchase prompt is not present on premium lessons for premium_beta users" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @premium_beta_user.email,
        password: "fixture-password"
      }
    }

    get addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
    get challenge_addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
  end

  test "the purchase prompt is not present on premium lessons for anonymous users visiting from a whitelisted IP address" do
    # The current_sign_in_ip for fixture users is always 127.0.0.1 and it can't be manually overridden
    # (at least, I wasn't able to find a way to override it).
    # Therefore, we'll test this by changing our whitelisted_ips fixture to 127.0.0.1 for just this test.
    whitelisted_ips(:ip_one).ip = "127.0.0.1"
    whitelisted_ips(:ip_one).save

    get addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
    get challenge_addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
  end

  test "the purchase prompt is not present on premium lessons for free users visiting from a whitelisted IP address" do
    post addin_user_session_path, params: {
      addin_user: {
        email: @free_user.email,
        password: "fixture-password"
      }
    }

    # The current_sign_in_ip for fixture users is always 127.0.0.1 and it can't be manually overridden
    # (at least, I wasn't able to find a way to override it).
    # Therefore, we'll test this by changing our whitelisted_ips fixture to 127.0.0.1 for just this test.
    whitelisted_ips(:ip_one).ip = "127.0.0.1"
    whitelisted_ips(:ip_one).save

    get addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
    get challenge_addin_course_lesson_path(2, 2)
    assert_select "#purchase-prompt", false
  end
end
