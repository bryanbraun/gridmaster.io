require 'test_helper'
require 'stripe_mock'

class AddinChargesTest < ActionDispatch::IntegrationTest
  def stripe_helper
    StripeMock.create_test_helper
  end

  def setup
    StripeMock.start
    @free_user = users(:bryan)
    @premium_user = users(:andrew)
  end

  def teardown
    StripeMock.stop
  end

  test "charge upgrades an existing free account when the email is in our system" do
    post addin_charges_path, params: {
      stripeEmail: @free_user.email,
      stripeToken: stripe_helper.generate_card_token
    }
    @free_user.reload

    assert_equal 'premium', @free_user.access_level
    assert @free_user.customer_id
    assert_equal I18n.t("charges.existing_account_upgraded", email: @free_user.email), flash[:success]

    assert_response :redirect
    assert_match %r{http://www.example.com/addin/purchase-complete}, @response.redirect_url
  end

  test "charge is cancelled when the account being upgraded is already premium" do
    post addin_charges_path, params: {
      stripeEmail: @premium_user.email,
      stripeToken: stripe_helper.generate_card_token
    }

    assert_equal I18n.t("charges.existing_account_already_upgraded", email: @premium_user.email), flash[:error]
    assert_redirected_to addin_courses_path
  end

  test "charge creates a new premium account when the email is not in our system" do
    nonregistered_email = 'nonregistered_user@example.com'
    post addin_charges_path, params: {
      stripeEmail: nonregistered_email,
      stripeToken: stripe_helper.generate_card_token
    }

    created_user = User.find_by(email: nonregistered_email)
    assert created_user
    assert_equal 'premium', created_user.access_level
    assert_equal I18n.t("charges.account_created_and_upgraded", email: nonregistered_email), flash[:success]
    assert_response :redirect
    assert_match %r{http://www.example.com/addin/purchase-complete}, @response.redirect_url
  end
end
