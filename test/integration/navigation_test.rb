require 'test_helper'

class NavigationTest < ActionDispatch::IntegrationTest

  test "header navigation" do
    get root_path
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", tips_path
    assert_select "a[href=?]", about_path
  end

end
