require "test_helper"

class WebsiteUsersTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bryan)
  end

  test "signing in sends you to the profile page" do
    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "fixture-password"
      }
    }

    assert_equal I18n.t("devise.sessions.signed_in"), flash[:notice]
    assert_redirected_to edit_user_registration_path
  end

  test "signing up sends you to the profile page" do
    post user_registration_path, params: {
      user: {
        email: "example@user.com",
        password: "fixture-password",
        password_confirmation: "fixture-password"
      }
    }

    assert_equal I18n.t("devise.registrations.signed_up"), flash[:notice]
    assert_redirected_to edit_user_registration_path
  end

  test "logging out sends you to the home page" do
    delete destroy_user_session_path

    assert_equal I18n.t("devise.sessions.signed_out"), flash[:notice]
    assert_redirected_to root_path
  end

  test "deleting your account sends you to the home page" do
    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "fixture-password"
      }
    }

    delete user_registration_path

    assert_equal I18n.t("devise.registrations.destroyed"), flash[:notice]
    assert_redirected_to root_path
  end

  test "basic info can be updated independently" do
    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "fixture-password"
      }
    }

    put users_update_info_path, params: {
      user: {
        name: "Bryan New Name",
        email: @user.email
      }
    }
    @user.reload

    assert_equal I18n.t("devise.registrations.update_info"), flash[:success]
    assert_equal "Bryan New Name", @user.name
  end

  test "password can be updated independently" do
    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "fixture-password"
      }
    }
    put users_update_password_path, params: {
      user: {
        current_password: "fixture-password",
        password: "new-fixture-password",
        password_confirmation: "new-fixture-password"
      }
    }
    @user.reload

    assert_equal I18n.t("devise.registrations.update_password"), flash[:success]
    assert !@user.valid_password?('fixture-password')
    assert @user.valid_password?('new-fixture-password')
  end

  test "password update failures display error messages" do
    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "fixture-password"
      }
    }
    put users_update_password_path, params: {
      user: {
        current_password: "incorrect password",
        password: "short",
        password_confirmation: "not matching"
      }
    }

    assert_select ".devise-validation > div", { text: /3 errors/ }
  end
end
