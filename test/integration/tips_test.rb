require 'test_helper'

class TipsTest < ActionDispatch::IntegrationTest

  def setup
    @tip1 = tips(:tip_one)
    @tip2 = tips(:tip_two)
    @tip3 = tips(:tip_three)
  end

  test "all published tips are listed on the index page" do
    get tips_path
    assert_select ".tiplist__tip", 3
  end

  test "tips on the index page are listed in descending order" do
    get tips_path
    assert_select ".tiplist__tip:first-of-type .tiplist__link[href=?]", tip_path(slug: @tip3.slug)
    assert_select ".tiplist__tip:last-of-type .tiplist__link[href=?]", tip_path(slug: @tip1.slug)
  end

  test "all of a tip's details are present on the show page" do
    get tip_path(slug: @tip1.slug)
    assert_select ".tip__title", @tip1.title
    assert_select ".tip__gif[src=?]", ActionController::Base.helpers.image_path(@tip1.image_url)
    assert_select ".tip__desc", { html: @tip1.description }
  end

end
