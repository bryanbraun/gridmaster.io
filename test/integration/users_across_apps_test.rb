require 'test_helper'

class UsersAcrossAppsTest < ActionDispatch::IntegrationTest

  # These tests ensure that a POST from the addin to the website returns people to the addin...
  #                  ...and a POST from the website to the addin returns people to the website.
  test "POSTing a successful password reset from the addin sends you back to the addin" do
    post user_password_path, params: {
       user: {
         email: "bryan@example.com",
       }
     },
     headers: {
       HTTP_REFERER: addin_users_password_new_path
     }

    assert_equal I18n.t("devise.passwords.send_instructions"), flash[:notice]
    assert_redirected_to addin_users_password_new_path
  end

  test "POSTing a successful password reset from the website sends you back to the website" do
    post user_password_path, params: {
       user: {
         email: "bryan@example.com",
       }
     },
     headers: {
       HTTP_REFERER: about_path
     }

    assert_equal I18n.t("devise.passwords.send_instructions"), flash[:notice]
    assert_redirected_to about_path
  end

  test "POSTing a successful confirmation link request from the addin sends you back to the addin" do
    post user_confirmation_path, params: {
       user: {
         email: "holly@example.com",
       }
     },
     headers: {
       HTTP_REFERER: addin_users_confirmation_new_path
     }

    assert_equal I18n.t("devise.confirmations.send_instructions"), flash[:notice]
    assert_redirected_to addin_users_confirmation_new_path
  end

  test "POSTing a successful confirmation link request from the website sends you back to the website" do
    post user_confirmation_path, params: {
       user: {
         email: "holly@example.com",
       }
     },
     headers: {
       HTTP_REFERER: about_path
     }

    assert_equal I18n.t("devise.confirmations.send_instructions"), flash[:notice]
    assert_redirected_to about_path
  end

end
