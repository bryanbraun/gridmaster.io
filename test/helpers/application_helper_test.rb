require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "whether the current page is in the addin" do
    # Addin pages
    assert addin_page? 'https://gridmaster.io/addin'
    assert addin_page? '/addin/user/login'
    assert addin_page? 'addin/user/login'

    # Non-addin pages
    assert !(addin_page? 'https://gridmaster.io/user/login')
    assert !(addin_page? 'https://gridmaster.io/')
    assert !(addin_page? '/')
  end

  test "can extract last segment of current url" do
    assert_equal 'courses', last_segment_of_current_url('https://gridmaster.io/addin/courses/')
    assert_equal 'courses', last_segment_of_current_url('https://gridmaster.io/addin/courses')

    # I don't _need_ this to be nil, but it currently is and that's ok, so I'll test for it.
    assert_nil last_segment_of_current_url 'https://gridmaster.io/'
  end

  test "can extract segment 1 level above current url" do
    assert_equal 'https://gridmaster.io/addin', url_a_level_above_current_url('https://gridmaster.io/addin/courses')
  end

  test "correct classes are applied for all flash levels in the addin" do
    assert_equal "ms-MessageBar--info", addin_flash_class(:notice)
    assert_equal "ms-MessageBar--success", addin_flash_class(:success)
    assert_equal "ms-MessageBar--error", addin_flash_class(:error)
    assert_equal "ms-MessageBar--warning", addin_flash_class(:alert)

    assert_equal "ms-Icon--Info", addin_flash_icon(:notice)
    assert_equal "ms-Icon--Completed", addin_flash_icon(:success)
    assert_equal "ms-Icon--ErrorBadge", addin_flash_icon(:error)
    assert_equal "ms-Icon--Info", addin_flash_icon(:alert)
  end

end
