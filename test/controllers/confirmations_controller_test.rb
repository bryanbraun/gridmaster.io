require 'test_helper'

class Users::ConfirmationsControllerTest < ActionDispatch::IntegrationTest

  test "Clicking a bad confirmation link redirects you to the home page with the appropriate message" do
    get '/users/confirmation?confirmation_token=abcdef'

    assert_equal "Confirmation token is invalid", flash[:error]
    assert_redirected_to root_url
  end

  test "Clicking a good confirmation link redirects you to the home page with the appropriate message" do
    get '/users/confirmation?confirmation_token=abcdefg1234567'

    assert_equal I18n.t("devise.confirmations.confirmed"), flash[:notice]
    assert_redirected_to root_url
  end

end
