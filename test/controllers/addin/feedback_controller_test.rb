require 'test_helper'

class Addin::FeedbackControllerTest < ActionDispatch::IntegrationTest
  test "unauthenticated feedback submissions should redirect" do
    post addin_feedback_create_url, params: {
       feedback: {
         email: "bryan@example.com",
         message: "example message"
       }
     }
    assert_response :redirect
  end
end
