require 'test_helper'

class Addin::Users::PasswordsControllerTest < ActionDispatch::IntegrationTest

  test "render custom passwords form" do
    get addin_users_password_new_path
    assert_response :success
    assert_template :new
  end

end
