require 'test_helper'

class Addin::Users::ConfirmationsControllerTest < ActionDispatch::IntegrationTest

  test "render custom confirmations form" do
    get addin_users_confirmation_new_path
    assert_response :success
    assert_template :new
  end

end
