require 'test_helper'

class BasicPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get "/"
    assert_response :success
  end

  test "should get about" do
    get "/about"
    assert_response :success
  end

  test "should get privacy" do
    get "/privacy"
    assert_response :success
  end

  test "should get addin-support" do
    get "/addin-support"
    assert_response :success
  end
end
