require 'test_helper'

class TipsControllerTest < ActionDispatch::IntegrationTest
  test "we can reach the tips index page" do
    get tips_path, headers: { "HTTP_ACCEPT" => "text/html" }
    assert_response :success
    assert_equal "text/html", @response.media_type
  end

  test "we can reach the tips RSS feed" do
    get tips_path, headers: { "HTTP_ACCEPT" => "application/rss+xml" }
    assert_response :success
    assert_equal "application/rss+xml", @response.media_type
  end

  test "we can request an individual tip with a slug parameter" do
    tip1 = tips(:tip_one)
    get tip_path(slug: tip1.slug)
    assert_equal tip1, @controller.instance_variable_get("@tip")
  end
end
