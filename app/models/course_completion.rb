class CourseCompletion < ApplicationRecord
  belongs_to :user

  validates :user_id, presence: true
  validates :course_id, presence: true
end
