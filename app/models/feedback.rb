class Feedback < MailForm::Base
  # Required fields
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i

  # Optional fields
  attribute :name
  attribute :option # for select list or radios
  attribute :message
  attribute :metadata

  attribute :nickname,  :captcha  => true

  def headers
    {
      :subject => "Gridmaster Feedback",
      :to => "info@gridmaster.io",
      :from => %("#{name}" <#{email}>)
    }
  end
end
