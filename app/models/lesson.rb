class Lesson < ApplicationRecord
  belongs_to :course, inverse_of: :lessons, :counter_cache => true
  before_save :convert_challenge_data_to_json

  # A run-time attribute for passing completion data to the view.
  # Not intended to be saved to the database.
  attr_accessor :completed

  private
    def convert_challenge_data_to_json
      if (challenge_data and challenge_data.is_a? String)
        self.challenge_data = JSON.parse(challenge_data)
      end
    end
end
