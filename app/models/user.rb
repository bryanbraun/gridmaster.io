class User < ApplicationRecord
  has_many :course_completions
  has_many :lesson_completions
  has_many :challenge_completions

  # Free    - The default. Gives you access to all free courses.
  # Premium - Gives you access to all paid courses.
  # Premium Beta - Gives you access to all courses, and ability to beta-test upcoming courses.
  enum access_level: [:free, :premium, :premium_beta]

  # Include default devise modules. Others available are:
  #   :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable, :confirmable

  def can_access_premium_courses?
    premium? || premium_beta? || visiting_from_whitelisted_ip_address?
  end

  def visiting_from_whitelisted_ip_address?
    whitelisted_ips = WhitelistedIp.all
    whitelisted_ips.each do |whitelisted_ip|
      return true if whitelisted_ip.ip.include?(current_sign_in_ip)
    end

    false
  end

  # Given an email address, generate a temporary account
  # with a password that can be changed later.
  def self.create_temp_account(email, attributes={})
    temp_password = Devise.friendly_token
    all_attributes = attributes.merge({
      email: email,
      password: temp_password,
      password_confirmation: temp_password
    })
    newUser = User.new(all_attributes)
    newUser.skip_confirmation_notification!  # skip the default confirmation email
    newUser.save
    newUser
  end

  # Based on the 'confirmable' method "send_confirmation_instructions"
  # and the 'recoverable' method "send_reset_password_instructions"
  def send_confirmation_and_reset_instructions
    unless @raw_confirmation_token
      generate_confirmation_token!
    end

    reset_password_token = set_reset_password_token

    opts = pending_reconfirmation? ? { to: unconfirmed_email } : { }
    send_devise_notification(:confirmation_and_reset_instructions, @raw_confirmation_token, reset_password_token, opts)
  end

end
