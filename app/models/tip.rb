class Tip < ApplicationRecord
  before_save :set_published_at, if: :published_changed?

  def set_published_at
    if self.published == true
      self.published_at = DateTime.now
    else
      self.published_at = nil
    end
  end

  def next
    self.class.where("published_at > ?", published_at).first
  end

  def previous
    self.class.where("published_at < ?", published_at).last
  end

end
