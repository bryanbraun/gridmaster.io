class Course < ApplicationRecord
  enum tier: [:free, :premium, :coming]
  has_many :lessons, inverse_of: :course

  # A run-time attribute for passing completion data to the view.
  # Not intended to be saved to the database.
  attr_accessor :completed
end
