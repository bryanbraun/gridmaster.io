# See: https://github.com/plataformatec/devise/wiki/How-To:-Use-custom-mailer
class CustomDeviseMailer < Devise::Mailer
  helper :application                      # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers  # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer'   # to make sure that your mailer uses the devise views

  def confirmation_and_reset_instructions(record, confirmation_token, reset_password_token, opts={})
    @confirmation_token = confirmation_token
    @reset_password_token = reset_password_token
    devise_mail(record, :confirmation_and_reset_instructions, opts)
  end
end
