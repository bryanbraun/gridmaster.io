# app/views/articles/index.rss.builder

xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Spreadsheet Tips"
    xml.description "Useful spreadsheet tips, as animated gifs"
    xml.link tips_url

    # 10 is arbitrary but we're going with it.
    @tips.first(10).each do |tip|
      xml.item do
        xml.title tip.title
        xml.description do
          xml.text! image_tag(image_url(tip.image_url), alt: tip.image_alt_text)
          if tip.description
            xml.text! tip.description
          end
        end
        xml.pubDate tip.published_at.to_s(:rfc822)
        xml.link tip_url(slug: tip.slug)
        xml.guid tip_url(slug: tip.slug)
      end
    end
  end
end
