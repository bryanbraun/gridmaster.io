class TipsController < ApplicationController

  def index
    @tips = Tip.where(published: true).order('published_at DESC')

    respond_to do |format|
      format.html
      format.rss { render :layout => false }
    end
  end

  def show
    @tip = Tip.find_by(published: true, slug: params[:slug]) or not_found
  end

end
