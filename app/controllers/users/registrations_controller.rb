# MANY OF THESE ACTIONS ARE SIMILAR TO THE CUSTOM ACTIONS IN:
#   /controllers/addin/users/registrations_controller.rb
class Users::RegistrationsController < Devise::RegistrationsController
  # Copy over `before_actions`, so I can add more actions without blowing away existing ones.
  prepend_before_action :require_no_authentication, only: [:new, :create, :cancel]
  prepend_before_action :authenticate_scope!, only: [:edit, :update, :destroy, :update_password, :update_info]
  prepend_before_action :set_minimum_password_length, only: [:new, :edit, :update_password]
  prepend_before_action :check_captcha, only: [:create]

  # I wanted to be able to update basic account details separately
  # from password so instead of using the default `update` action,
  # I'm creating my own `update_password` and `update_info` actions,
  # and reusing whatever bits make sense.
  def update
    redirect_to current_user
  end

  def update_password
    if current_user.update_with_password(user_password_params)
      flash[:success] = I18n.t("devise.registrations.update_password")
      bypass_sign_in(current_user)
      redirect_to action: :edit
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  def update_info
    if current_user.update_without_password(user_basic_params)
      flash[:success] = I18n.t("devise.registrations.update_info")
      redirect_to action: :edit
    else
      respond_with resource
    end
  end

  private
    def user_password_params
      params.require(:user).permit(:current_password, :password, :password_confirmation)
    end

    def user_basic_params
      params.require(:user).permit(:name, :email)
    end

    def check_captcha
      if !verify_recaptcha
        # Recaptcha failed. In other words, this might be a bot.
        self.resource = resource_class.new sign_up_params
        # Look for any other validation errors besides Recaptcha
        resource.validate
        clean_up_passwords resource
        set_minimum_password_length
        respond_with resource
      end 
    end
end
