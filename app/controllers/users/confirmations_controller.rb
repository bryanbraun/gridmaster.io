class Users::ConfirmationsController < Devise::ConfirmationsController

  # POST /resource/confirmation
  #
  # We customized this action to redirect requests back to whatever part of the site sent them.
  # This way, addin forms can post to a non-addin devise endpoint, and still generate emails with
  # the correct links to the main site. We needed to override the whole action (instead of just
  # `after_resending_confirmation_instructions_path_for`) in order to customize both the success
  # and failure cases.
  def create
    self.resource = resource_class.send_confirmation_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      redirect_back(fallback_location: root_path)
    else
      # We manually add the contents of resource.errors.full_messages to flash,
      # because they'd otherwise get lost in the redirect.
      flash[:error] = flash[:error].to_s.concat resource.errors.full_messages.join(" ")
      redirect_back(fallback_location: root_path)
    end
  end


  # GET /resource/confirmation?confirmation_token=abcdef
  #
  # We customized this action to always redirect to the gridmaster home page, and ensure
  # the proper flash messages are displayed. We needed to override the whole action (instead of
  # just `after_confirmation_path_for`) in order to customize both the success and failure cases
  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?

    if resource.errors.empty?
      set_flash_message!(:notice, :confirmed)
      redirect_to root_path
    else
      flash[:error] = flash[:error].to_s.concat resource.errors.full_messages.join(" ")
      redirect_to root_path
    end
  end

end
