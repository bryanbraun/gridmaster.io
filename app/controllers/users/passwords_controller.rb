class Users::PasswordsController < Devise::PasswordsController

  # POST /resource/password                (for submitting the "forgot your password" form)
  #
  # We customized this action to redirect requests back to whatever part of the site sent them.
  # This way, addin forms can post to a non-addin devise endpoint, and still generate emails with
  # the correct links to the main site. We needed to override the whole action (instead of just
  # `after_resending_confirmation_instructions_path_for`) in order to customize both the success
  # and failure cases.
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      redirect_back(fallback_location: root_path)
    else
      # We manually add the contents of resource.errors.full_messages to flash,
      # because they'd otherwise get lost in the redirect.
      flash[:error] = flash[:error].to_s.concat resource.errors.full_messages.join(" ")
      redirect_back(fallback_location: root_path)
    end
  end
end
