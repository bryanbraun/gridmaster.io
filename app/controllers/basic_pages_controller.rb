class BasicPagesController < ApplicationController
  layout false, only: [:join_tips]

  def home
  end

  def about
  end

  def tips
  end

  def pricing
  end

  def pricing_updates
  end

  def purchase_complete
  end

  def libraries
  end

  def community_centers
  end

  def cities
    @city = City.find_by(slug: params['slug'])
    render :layout => 'application-simple.html.erb'
  end

  def training
    render :layout => 'application-simple.html.erb'
  end

  # I'm using this page as a "support" page listed in the add-in manifest file.
  # This is a requirment for submitting to the microsoft office store:
  #   "Provide the URL to support customers who have issues with your app."
  def addin_support
  end

  def privacy
  end

  # Currently this page does nothing. We only set it up as a place to send people
  # to from one mailchimp list, so we could manually add them to another mailchimp
  # list if they click through (as determined by analytics). We can remove it in
  # the future.
  def join_tips
  end
end
