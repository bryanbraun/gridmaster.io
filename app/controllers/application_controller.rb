class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, prepend: true

  # Redirect to the profile page after login.
  #
  # This function is overridden by one of the same name in addin
  # controllers, providing different redirects in those situations.
  # See `controllers/concerns/addin_setup.rb` for details.
  def after_sign_in_path_for(resource_or_scope)
    edit_user_registration_path
  end

  # A shorthand method for routing the request to the 404 page.
  # See https://stackoverflow.com/a/4983354/1154642
  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
end
