module AddinSetup
  # Setting up the addin's layout and headers used to be in a base controller
  # inherited by the other controllers, but I moved it here because I
  # needed something more flexible for devise, which couldn't inherit the
  # base controller in a non-global manner.
  extend ActiveSupport::Concern

  included do
    layout 'addin'
    before_action :set_addin_headers

    # saves the location before loading each page so we can return to the
    # right page. If we're on a devise page, we don't want to store that as the
    # place to return to (for example, we don't want to return to the sign in page
    # after signing in), which is what the :unless prevents
    before_action :store_current_location, :unless => :devise_controller?
  end

  private
    def set_addin_headers
      response.headers['x-frame-options'] = 'ALLOW-ALL'
    end

    # Override the devise helper to store the current location so we can
    # redirect to it after logging in or out. This override makes signing in
    # and signing up work automatically.
    def store_current_location
      store_location_for(:addin_user, request.url) if request.get?
    end

    # Store a redirect location for returning to the step after a completed challenge step.
    # We're keeping this in this concern because 1) it's used by both the SessionController and
    # RegistrationsController 2) it uses the request object and 3) it'll be with other
    # loaction storing fuctions.
    def set_redirect_to_next_challenge_step(completion_params)
      order_in_course = Lesson.find(completion_params[:lesson_id]).order_in_course
      next_challenge_step = completion_params[:challenge_step].to_i + 1
      new_redirect_url = URI::HTTPS.build(
        host: request.host,
        path: "/addin/courses/#{completion_params[:course_id]}/lessons/#{order_in_course}/challenge",
        fragment: next_challenge_step.to_s
      )
      store_location_for(:addin_user, new_redirect_url.to_s)
    end

    # Override Devise's redirect helpers. See notes in the code here:
    # https://github.com/plataformatec/devise/blob/88724e10adaf9ffd1d8dbfbaadda2b9d40de756a/lib/devise/controllers/helpers.rb
    # Currently, this handles both sign_up and sign_in
    def after_sign_in_path_for(resource_or_scope)
      stored_location_for(:addin_user) || addin_courses_path
    end

    # This is an edge-case that might never happen but we define it just in case
    def after_inactive_sign_up_path_for(resource)
      new_addin_user_registration_path
    end

    # Currently, this handles both sign_out and delete account
    def after_sign_out_path_for(resource_or_scope)
      addin_courses_path
    end
end
