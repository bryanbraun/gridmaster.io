class Addin::FeedbackController < ApplicationController
  # Prevent non-authenticated users from POSTing to feedback endpoints.
  before_action :authenticate_addin_user!

  # This action is designed to receive generic feedback. You can override the default redirect
  # (which points back at your last page) by adding a hidden field where name="redirect" and
  # value="/my/relative/path".
  def create
    @feedback = Feedback.new(params[:feedback])
    @feedback.request = request
    if @feedback.deliver
      flash[:success] = "You know what? You're awesome, that's what! Big thanks for the feedback!"
    else
      flash[:error] = "Well this is awkward. Something went wrong and we can't send your feedback right now. Try emailing us at info@gridmaster.io."
    end

    redirect_to params[:redirect] || stored_location_for(:addin_user) || addin_courses_path
  end

  # The challenge feedback form is AJAX submitted, so we have a separate action that handles things differently.
  def createChallenge
    @feedback = Feedback.new(params[:feedback])
    @feedback.request = request
    if @feedback.deliver
      flash.now[:success] = "You know what? You're awesome, that's what! Big thanks for the feedback!"
    else
      flash.now[:error] = "Well this is awkward. Something went wrong and we can't send your feedback right now. Try emailing us at info@gridmaster.io."
    end

    render :template => "addin/shared/_flash", :layout => false
  end


end
