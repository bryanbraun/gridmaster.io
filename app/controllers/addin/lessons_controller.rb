class Addin::LessonsController < ApplicationController
  include AddinSetup

  # /addin/courses/:id/lessons
  def index
    @user = current_addin_user_or_temp_user
    @course = Course.find_by(id: params['course_id']) or not_found
    @lessons = Lesson.where(course_id: params['course_id']).order(:order_in_course)

    # Add a completion flag to each lesson
    completedLessons = LessonCompletion.where(user_id: @user.id, course_id: @course.id).distinct.pluck(:lesson_id)
    @lessons.each do |lesson|
      lesson.completed = completedLessons.include? lesson.id
    end
  end

  # /addin/courses/:id/lessons/:number_in_course
  def show
    @user = current_addin_user_or_temp_user
    @lesson = Lesson.where(course_id: params['course_id']).find_by(order_in_course: params['number_in_course']) or not_found
  end

  # /addin/courses/:id/lessons/:number_in_course/fullscreen
  def fullscreen
    @lesson = Lesson.where(course_id: params['course_id']).find_by(order_in_course: params['number_in_course']) or not_found
    render :layout => 'addin-fullscreen-video'
  end

  # /addin/courses/:id/lessons/:number_in_course/challenge
  def challenge
    @user = current_addin_user_or_temp_user
    @lesson = Lesson.where(course_id: params['course_id']).find_by(order_in_course: params['number_in_course']) or not_found
    @lessonTotal = Course.find(params['course_id']).lessons.size

    # Add a completion flag to each challenge step
    completedSteps = ChallengeCompletion.where(user_id: @user.id, lesson_id: @lesson.id).distinct.pluck(:challenge_step)
    @lesson.challenge_data['steps'].each_with_index do |step, index|
      step['completed'] = completedSteps.include? index + 1
    end

    # Ensure the minimum_password_length info is available to the sign-up form.
    set_minimum_password_length
  end

  private
    def current_addin_user_or_temp_user
      # By populating the request.remote_ip, we ensure that anonymous users can be IP whitelisted too.
      addin_user_signed_in? ? current_addin_user : User.new(current_sign_in_ip: request.remote_ip)
    end

    # Devise methods used for login/signup prompts on lesson pages.
    def resource_name
      :addin_user
    end
    helper_method :resource_name

    def resource
      @resource ||= User.new
    end
    helper_method :resource

    def devise_mapping
      @devise_mapping ||= Devise.mappings[:addin_user]
    end
    helper_method :devise_mapping

    def resource_class
      User
    end
    helper_method :resource_class

    def set_minimum_password_length
      if devise_mapping.validatable?
        @minimum_password_length = resource_class.password_length.min
      end
    end

end
