class Addin::CompletionsController < ApplicationController # DeviseController?
  # Prevent non-authenticated users from POSTing to the basic create endpoint.
  # This is what prompts users to create an account to not lose their progress.
  before_action :authenticate_addin_user!

  def create
    data_response = CompletionService.new.save_progress(challenge_completion_params)
    render :json => data_response
  end

  private
    def challenge_completion_params
      params.require(:completion).permit(:user_id, :course_id, :lesson_id, :challenge_step)
    end
end
