class Addin::Users::SessionsController < Devise::SessionsController
  include AddinSetup

  # Enhance the default `sessions#new` action with the ability to add completion_data to the form.
  # We do this in case the person landing on this form was redirected from a completed challenge.
  def new
    if session[:completion_data]
      @completion_data = session[:completion_data]
      session[:completion_data] = nil
    end

    super
  end

  # Update the existing sessions#create to record completions, if any completion data exists in the form.
  def create
    if params[:completion]
      # We set and unset session data so we can preserve completion information in case
      # authentication fails and we get redirected to a form with validation messages.
      session[:completion_data] = params[:completion]
      set_redirect_to_next_challenge_step(params[:completion])
    end

    resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#new")

    set_flash_message!(:notice, :signed_in)
    sign_in(resource_name, resource)

    # If we made it here then login was successful, so we can unset session data and record completions.
    if params[:completion]
      session[:completion_data] = nil
      CompletionService.new.save_progress(
        user_id: resource.id,
        course_id: params[:completion][:course_id],
        lesson_id: params[:completion][:lesson_id],
        challenge_step: params[:completion][:challenge_step]
      )
    end

    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end

end
