# See original registration controller:
#   https://github.com/plataformatec/devise/blob/71fc5b351a8e8473733b0ca29d60c06e43a0efe1/app/controllers/devise/registrations_controller.rb
#
# Notes on overriding:
#   We've namespaced our resource, which changes `user` --> `addin_user` in tons of places (like
#   names for helpers, methods, variables, and more). Thus, any instructions you find online may
#   need to be adjusted accordingly. Here are some examples of changed values:
#    `authenticate_user` --> `authenticate_addin_user`
#    `current_user` --> `current_addin_user`
#
# Useful values from the parent controller:
#   `resource`      - Devise's abstract variable for our resource. In our case, we can access the
#                     same resource with `current_addin_user`, which is more expressive.
#   `resource_name` - Devise's variable for your resource name. Ours is `:addin_user` instead of
#                     `:user` because we are namespacing.
class Addin::Users::RegistrationsController < Devise::RegistrationsController
  include AddinSetup
  # Copy over `before_actions`, so I can add more actions without blowing away existing ones.
  prepend_before_action :require_no_authentication, only: [:new, :create, :cancel]
  prepend_before_action :authenticate_scope!, only: [:edit, :update, :destroy, :update_password, :update_info]
  prepend_before_action :set_minimum_password_length, only: [:new, :edit, :update_password]
  prepend_before_action :check_captcha, only: [:create]

  # Update the existing registrations#create to record completions, if any completion
  # data exists in the form. All the new stuff is inside a "if params[:completion]".
  def create
    if params[:completion]
      # Set the stored redirect location to a challenge url with the next challenge step
      set_redirect_to_next_challenge_step(params[:completion])
    end

    build_resource(sign_up_params)
    resource.save
    yield resource if block_given?

    if resource.persisted?
      if resource.active_for_authentication?

        # If we made it here then sign up was successful, so we can record completions.
        if params[:completion]
          CompletionService.new.save_progress(
            user_id: resource.id,
            course_id: params[:completion][:course_id],
            lesson_id: params[:completion][:lesson_id],
            challenge_step: params[:completion][:challenge_step]
          )
        end

        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      # The sign up failed, so we set the completion data to a class variable allowing it
      # to be pre-populated on the next sign up form that gets rendered.
      if params[:completion]
        @completion_data = params[:completion]
      end

      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource, location: new_addin_user_registration_path
    end
  end

  # I wanted to be able to update basic account details separately
  # from password so instead of using the default `update` action,
  # I'm creating my own `update_password` and `update_info` actions,
  # and reusing whatever bits make sense.
  def update
    redirect_to current_addin_user
  end

  def update_password
    if current_addin_user.update_with_password(user_password_params)
      flash[:success] = I18n.t("devise.registrations.update_password")
      bypass_sign_in(current_addin_user)
      redirect_to action: :edit
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  def update_info
    if current_addin_user.update_without_password(user_basic_params)
      flash[:success] = I18n.t("devise.registrations.update_info")
      redirect_to action: :edit
    else
      respond_with resource
    end
  end

  private
    def user_password_params
      params.require(:addin_user).permit(:current_password, :password, :password_confirmation)
    end

    def user_basic_params
      params.require(:addin_user).permit(:name, :email)
    end

    def check_captcha
      if !verify_recaptcha
        # Recaptcha failed. In other words, this might be a bot.
        self.resource = resource_class.new sign_up_params
        # Look for any other validation errors besides Recaptcha
        resource.validate
        clean_up_passwords resource
        set_minimum_password_length
        respond_with resource
      end
    end

end
