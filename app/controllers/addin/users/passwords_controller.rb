# This is a custom "semi-devise" controller. It's just for setting up a form,
# that POSTs to devise-powered endpoints on the main website. It inherits from
# a parent devise controller so we can access some useful varibles in our form.
class Addin::Users::PasswordsController < DeviseController
  include AddinSetup

  def new
    self.resource = resource_class.new
  end

end
