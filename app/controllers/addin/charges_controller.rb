class Addin::ChargesController < ApplicationController

  # Create charges for transactions in the addin. It's similar to the controller on the main
  # website, but it has addin-specific redirects.
  def create
    stripeEmail = params[:stripeEmail]
    existing_user = User.find_by(email: stripeEmail)

    if existing_user && !existing_user.free?
      # The user was previously upgraded and shouldn't be charged to get upgraded again.
      flash[:error] = I18n.t("charges.existing_account_already_upgraded", email: stripeEmail)
      redirect_to addin_courses_path and return
    end

    customer = Stripe::Customer.create(
      email: stripeEmail,
      source: params[:stripeToken]
    )

    amount = 7900

    charge = Stripe::Charge.create(
      customer: customer.id,
      amount: amount,
      description: 'Gridmaster Premium',
      currency: 'usd'
    )

    attributes = { customer_id: customer.id, access_level: 'premium' }

    if existing_user
      existing_user.assign_attributes(attributes)
      existing_user.save
      flash[:success] = I18n.t("charges.existing_account_upgraded", email: stripeEmail)
    else
      new_user = User.create_temp_account(stripeEmail, attributes)
      new_user.send_confirmation_and_reset_instructions
      sign_in(:addin_user, new_user)
      flash[:success] = I18n.t("charges.account_created_and_upgraded", email: stripeEmail)
    end

    redirect_to addin_purchase_complete_path(:transaction_id => charge.id)
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to addin_courses_path
  end
end
