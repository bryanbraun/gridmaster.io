class Addin::BasicPagesController < ApplicationController
  include AddinSetup

  def index
    redirect_to addin_courses_path
  end

  def purchase_complete
  end
end
