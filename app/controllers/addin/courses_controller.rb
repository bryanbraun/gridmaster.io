class Addin::CoursesController < ApplicationController
  include AddinSetup

  # /addin/courses
  def index
    @user = addin_user_signed_in? ? current_addin_user : User.new
    @courses = Course.order(:id).all

    # Add a completion flag to each lesson
    completedCourses = CourseCompletion.where(user_id: @user.id).distinct.pluck(:course_id)
    @courses.each do |course|
      course.completed = completedCourses.include? course.id
    end
  end

  # /addin/course/:id
  def show
    # This action is not currently being used, so we just redirect to the list of
    # all courses. This works well for our "up a level" button helper.
    redirect_to action: :index
  end

  # /addin/courses/:id/conclusion
  def conclusion
    @user = addin_user_signed_in? ? current_addin_user : User.new
    @course = Course.find(params['id'])
  end

end
