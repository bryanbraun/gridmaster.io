class ChargesController < ApplicationController
  def create
    stripeEmail = params[:stripeEmail]
    existing_user = User.find_by(email: stripeEmail)

    if existing_user && !existing_user.free?
      # The user was previously upgraded and shouldn't be charged to get upgraded again.
      flash[:error] = I18n.t("charges.existing_account_already_upgraded", email: stripeEmail)
      redirect_back(fallback_location: pricing_path) and return
    end

    customer = Stripe::Customer.create(
      email: stripeEmail,
      source: params[:stripeToken]
    )

    amount = 7900

    charge = Stripe::Charge.create(
      customer: customer.id,
      amount: amount,
      description: 'Gridmaster Premium',
      currency: 'usd'
    )

    attributes = { customer_id: customer.id, access_level: 'premium' }

    if existing_user
      existing_user.assign_attributes(attributes)
      existing_user.save
      flash[:success] = I18n.t("charges.existing_account_upgraded", email: stripeEmail)
    else
      new_user = User.create_temp_account(stripeEmail, attributes)
      new_user.send_confirmation_and_reset_instructions
      sign_in(new_user)
      flash[:success] = I18n.t("charges.account_created_and_upgraded", email: stripeEmail)
    end

    redirect_to purchase_complete_path(:transaction_id => charge.id)
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_back(fallback_location: pricing_path)
  end
end
