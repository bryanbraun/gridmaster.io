// This is a manifest file that bundles all the JS needed for the addin.
//
// VENDOR
//= require modernizr-2.8.3.min
//= require jquery
//= require jquery_ujs
//= require addin/debounce
//= require addin/domConfetti
//= require player.min
// -- add vendor addin files here --
//
// CUSTOM
//= require main
//= require hard-post
//= require ./addin/initializer
//= require_tree ./addin
