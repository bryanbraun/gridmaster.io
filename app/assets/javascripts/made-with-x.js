(function() {
  function randomizeEmoji(selector) {
    var icons = [
        "✨",
        "⚡️",
        "💻",
        // '🎥', unclear on a dark background
        "🔧",
        "💖",
        "💪",
        "⌛️",
        "💡",
        // '🔨', unclear on a dark background
        "🔬",
        "💛",
        "💚",
        "💙",
        "💜",
        "🙇",
        "🎯",
        // '⛏', unclear on a dark background
        "📹",
        "🖥",
        "🌈",
        "🍀"
      ],
      emojiWrapper = document.querySelector(selector);

    if (emojiWrapper) {
      var chosenIcon = icons[Math.floor(Math.random() * icons.length)];
      emojiWrapper.innerHTML = chosenIcon;
    }
  }

  randomizeEmoji(".made-with-emoji");
})();
