/**
 * For printing, updating, and clearing challenge messages from the addin.
 */
GM.challengeMessages = (function() {
  var messages = {
    starting: ["Don't worry... you've got this."],
    mistake: [
      "Whoops, that's not what we're looking for.",
      "Hmm, that's not the answer we're expecting",
      "It looks like something's incorrect. Can you double check?"
    ],
    progress: [
      'Doing great. Keep up the good work!',
      'So far, so good.',
      "That's the idea!",
      'Looking good, so far.'
    ],
    completion: [
      'You did it! Onward!',
      'You got it!',
      "That's it! Nicely done.",
      'There we go!',
      'Nailed it!',
      'Well done! On we go.',
      "I didn't doubt you for a second.",
      'BOOM! Got it.'
    ],
    error: ['Whoops. Something went wrong.'],
    randomlyChoose: function(type) {
      var randomIndex = Math.floor(Math.random() * this[type].length);
      return this[type][randomIndex];
    }
  };
  var debouncedShowQueuedChallengeMessage = debounce(
    showQueuedChallengeMessage,
    150
  );

  /**
   * Displays a MessageBar with the provided message.
   * @param  {String} type - The type of challenge message you want to show.
   *                         Can be: "starting", "mistake", "progress", "completion", or "error".
   * @param  {String} dataMessage - The message to be printed in the messageBar, provided by the data.
   * @param  {Boolean} debounced - Whether the message should be debounced with other messages. Defaults to true.
   */
  function printChallengeMessage(type, dataMessage, debounced) {
    var isDebounced = debounced || true;
    var messageBarMap = {
      starting: 'info',
      mistake: 'error',
      progress: 'info',
      completion: 'success',
      error: 'error'
    };

    removeExistingChallengeMessages();

    var message = dataMessage ? dataMessage : messages.randomlyChoose(type);
    $(
      '.challenge-message.ms-MessageBar--' +
        messageBarMap[type] +
        ' .ms-MessageBar-text'
    ).append(message);
    $('.challenge-message.ms-MessageBar--' + messageBarMap[type]).addClass(
      'challenge-message--queued'
    );

    // Debounce prevents multiple messages from showing/flashing at once, when multiple cells
    // are tested at the same time. Only the last message in a batch will be shown.
    if (isDebounced) {
      debouncedShowQueuedChallengeMessage();
    } else {
      showQueuedChallengeMessage;
    }
  }

  function removeExistingChallengeMessages() {
    $('.challenge-message .ms-MessageBar-text').empty();
    $('.challenge-message').removeClass(
      'challenge-message--visible challenge-message--queued'
    );
  }

  function showQueuedChallengeMessage() {
    $('.challenge-message--queued').addClass('challenge-message--visible');
  }

  return {
    printChallengeMessage: printChallengeMessage,
    removeExistingChallengeMessages: removeExistingChallengeMessages
  };
})();
