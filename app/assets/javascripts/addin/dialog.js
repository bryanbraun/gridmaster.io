/**
 * Functionality for using Excel's dialog window
 * (ex: for playing larger videos, etc)
 */
GM.dialog = (function () {
  var dialogObj = {},
      isSupported;

  function open(URL, h, w) {
    console.log(URL, h, w);
    var opts = {
      height: h,
      width: w,
      displayInIframe: true
    };

    Office.context.ui.displayDialogAsync(URL, opts, dialogCallback);
  }

  function dialogCallback(asyncResult) {
    if (asyncResult.status == "failed") {
      console.log('Async request failed.');
      console.log(asyncResult.error.message);
    }
    else {
      dialogObj = asyncResult.value;
      /*Messages are sent by developers programatically from the dialog using office.context.ui.messageParent(...)*/
      dialogObj.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogMessageReceived, messageHandler);

      /*Events are sent by the platform in response to user actions or errors. For example, the dialog is closed via the 'x' button*/
      dialogObj.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogEventReceived, eventHandler);
    }
  }

  function messageHandler(arg) {
    dialogObj.close();
  }

  function eventHandler(arg) {
    switch (arg.error) {
      case 12002:
        console.log("Cannot load URL, 404 not found?");
        break;
      case 12003:
        console.log("Invalid URL Syntax");
        break;
      case 12004:
        console.log("Domain not in AppDomain list");
        break;
      case 12005:
        console.log("HTTPS Required");
        break;
      case 12006:
        console.log("Dialog closed");
        break;
      case 12007:
        console.log("Dialog already opened");
        break;
    }
  }

  function init() {
    isSupported = Office.context.requirements.isSetSupported('DialogApi', 1.1);
  }

  return {
    open: open,
    isSupported: isSupported,
    init: init
  };
})();
