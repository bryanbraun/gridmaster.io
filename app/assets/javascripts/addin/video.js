/**
 * Code that enhances video instruction
 * @return {Object} - Public methods.
 */
GM.video = (function() {
  var fullscreenLink = document.querySelector(".video-popup-link");

  /**
   * Click Event handler, triggering the video popup dialog.
   * @memberof
   * @param    {Event} event [description]
   */
  function launchVideoInDialog(event) {
    event.preventDefault();

    // For some reason, 10% widths look good on Excel 2016 for mac... they look more
    // like 50% or 80% on other platforms. I'm going to default to better experience
    // on Windows / online by using larger %'s' that make it oversized on Mac.
    //
    // "Excel for Mac" optimized
    // var height = 10;
    // var width = 10;

    // "Excel for (Windows/online/??)" optimized
    var height = 80;
    var width = 80;
    var videoUrl = event.currentTarget.href;
    var lessonPlayerIframe = document.querySelector(".lesson-videoplayer iframe");

    if (lessonPlayerIframe && Vimeo) {
      vimeoPlayer = new Vimeo.Player(lessonPlayerIframe);
      vimeoPlayer.pause();
    }

    GM.dialog.open(videoUrl, height, width);
  }

  function init() {
    if (fullscreenLink) fullscreenLink.addEventListener("click", launchVideoInDialog);
  }

  return {
    init: init
  }
})();

GM.initializer.register(GM.video.init);
