GM.challengeFeedback = (function() {
  /**
   * Populates the hidden metadata field with an updated url
   * (containing the hash value for the current step).
   */
  function updateFeedbackMetadata() {
    $('#feedback_metadata').val(location.href);
  }

  /**
   * On successful AJAX form submission, show the appropriate flash message.
   * @param    {Event} e       Event object
   * @param    {String} data   Content returned form the controller
   * @param    {String} status Request status
   * @param    {Object} xhr    XHR Object
   */
  function handleSuccessfulPOST(e, data, status, xhr) {
    $(this)[0].reset();
    $('#feedback.modal').hide();
    $('.addin-flash-wrap').remove();
    $('.ms-landing-page__main').prepend(data);
  }

  /**
   * On failed AJAX form submission, shows the predefined, static, error message.
   * @todo: log errors like these somewhere, like a rollbar.
   *
   * @param    {Event} e       Event object
   * @param    {Object} xhr    XHR object
   * @param    {String} status Request status
   * @param    {String} error  Error message from the server (not user-friendly)
   */
  function handleFailedPOST(e, xhr, status, error) {
    $('.challenge-feedback-message.ms-MessageBar--error').show();
  }

  function init() {
    $('#challenge-feedback')
      .on('ajax:success', handleSuccessfulPOST)
      .on('ajax:error', handleFailedPOST);

    $('[data-modal-trigger-id]').click(updateFeedbackMetadata);
  }

  return {
    init: init
  };
})();

$(document).ready(GM.challengeFeedback.init);
