/**
 * Javascript for interactive flash behaviors
 * @todo Is this actively used?
 * @return {Object} - Public methods.
 */
GM.flash = (function() {
  function init() {

    // Behaviors for flash-message boxes.
    $('.flash').append('<a class="icon-cross" href="#0"></a>');

    $('.flash .icon-cross').click(function(event) {
      event.preventDefault();
      $(this).parent('.flash').fadeOut(1000, function(){
        $(this).css({"visibility":"hidden",display:'block'}).slideUp('fast');
      });
    });

  }

  return {
    init: init
  }
})();

// I don't need to wait until Office.js is ready to run this... I only need the DOM
// to be ready, so I'll run it on $(document).ready. This also has the benefit of
// having it working in non-office environments for navigation and debugging purposes.
$(document).ready(GM.flash.init);
