GM.challengeCompletion = (function() {

  /**
   * On successful AJAX form submission advance the challenge like normal.
   * (also hides any previous completion errors that might be showing)
   * @param    {Event} e       Event object
   * @param    {String} data   Content returned form the controller
   * @param    {String} status Request status
   * @param    {Object} xhr    XHR Object
   */
  function handleSuccessfulCompletionPOST(e, data, status, xhr) {
    if (data.updated === true) {
      var completedStepEl = document.querySelector(".challenge-nav-step:nth-of-type(" + data.updated_step_number + ")");
      completedStepEl.classList.add("is-completed");
    }

    GM.challenges.updateStep(GM.challenges.getCurrentStep() + 1);
    hideStaticError();
  }

  /**
   * Handle failed AJAX form submissions.
   * @todo: log static errors like these somewhere, like a rollbar?
   *
   * @param    {Event} e       Event object
   * @param    {Object} xhr    XHR object
   * @param    {String} status Request status
   * @param    {String} error  Error message from the server (not user-friendly)
   */
  function handleFailedCompletionPOST(e, xhr, status, error) {
    if (xhr.status === 401) {
      if (GM.challengeSaveProgress.suppressSaveYourProgressPrompt) {
        GM.challenges.updateStep(GM.challenges.getCurrentStep() + 1);
      } else {
        var currentStepInLoginForm = document.getElementById("log-in-and-save-progress--completed-step");
        var currentStepInSignupForm = document.getElementById("sign-up-and-save-progress--completed-step");

        currentStepInLoginForm.value = GM.challenges.getCurrentStep();
        currentStepInSignupForm.value = GM.challenges.getCurrentStep();
        GM.challengeSaveProgress.showSaveYourProgress();
      }
    } else {
      showStaticError();
    }
  }

  function showStaticError() {
    var serverErrorText = "Uh oh. Something went wrong and we can't save your progress right now. Feel free to continue working on challenges though.";
    $(".challenge-completion-error .ms-MessageBar-text").text(serverErrorText);
    $(".challenge-completion-error.ms-MessageBar--error").show();
  }

  function hideStaticError() {
    $(".challenge-completion-error.ms-MessageBar--error").hide();
  }

  /**
   * Trigger the loading animation before sending our completion request
   * to make it feel like something is happening immediately and improve
   * perceived performance.
   */
  function beforeSubmitCompletion() {
    GM.challenges.clearAddinInstructions();
  }

  function init() {
    // Events for AJAX form submissions
    $("#challenge-completion")
      .on("ajax:before", beforeSubmitCompletion)
      .on("ajax:success", handleSuccessfulCompletionPOST)
      .on("ajax:error", handleFailedCompletionPOST);
  }

  return {
    init: init,
    showStaticError: showStaticError
  };
})();

$(document).ready(GM.challengeCompletion.init);
