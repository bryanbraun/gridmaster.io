/**
 * Exercise Checker README
 *
 * Example Usage:
 *
 * // Make a checker instance
 * checker = new ExerciseChecker({}, { callback: alert, data: 'You passed all steps!' };
 *
 * // Add a check to the library. Each check can be generic and used multiple
 * // times across cells, or not at all. Each one should have a unique
 * // name, or it won't be added.
 * checker.addCheckToLibrary({
 *   name: 'beginsWithEquals'
 *   regex: '^=',
 *   successAction: function(data) { alert(data.successMsg); },
 *   failureAction: function(data) { alert(data.failureMsg); },
 *   data: {
 *   	 progressMessage: 'it begins with an equals',
 *   	 mistakeMessage: 'it doesn't begin with an equals'
 *   }
 * });
 *
 * // You also add an array of checks all at once.
 * checker.addChecksToLibrary(arrayOfChecks);
 *
 * // Provide a list of check names that you want to apply to a specific cell.
 * // The checks themselves should be added to the library first, but you can
 * // add new checks on the fly, as long as they include the regex.
 * // ORDER IS SIGNIFICANT. Checks will be run in the order listed and the
 * // validation will halt on the first check that fails.
 * var b2Checks = [
 * 		 { name: 'beginsWithEquals' },
 * 		 { name: 'containsSum',
 * 		   regex: 'SUM'
 * 		 }
 * 	 ]
 * checker.registerCriteria('B2', b2Checks);
 *
 * // With our critera defined, ExcerciseChecker provides us a validation function
 * // we can use with Handsontable.
 * defaultSettings = {
 * 	 data: [[]],
 * 	 formulas: true,
 * 	 allowInvalid: true,
 * 	 cell: {
 * 	 	 {
 * 	 	 	 row: 1,
 * 	 	 	 col: 1,
 * 	 	 	 validator: checker.getValidationFunction('B2');
 * 	 	 }
 * 	 }
 * }
 * tbl = new Handsontable(container, defaultSettings);
 *
 * // Notes:
 * // - When each check within a cellCriteria passes, its successAction fires.
 * // - When a check fails, the failureAction fires, and the validation halts (subsequent checks aren't tested).
 * // - When all checks have passed, across all registered criteria, the finalAction fires.
 */

/**
 * ExerciseChecker - an object for defining checks to validate success during an exercise.
 * It provides feedback on the correctness of an individual input, and reports completion
 * when all inputs are correct. Each exercise (spreadsheet) requires an exercise checker.
 *
 * Designed to be used with handsontable with formulas: true.
 *
 * @param {object} registrationDefaults - A an object containing defaults for each check we register..
 * @param {object} finalActionObj - An object containing a function to run when all checks pass validation (and any data
 *                                	that we want to pass into that function).
 */

function ExerciseChecker(registrationDefaults, finalActionObj) {
  var doNothing = function(){};

  this.finalActionObj = finalActionObj || { callback: doNothing, data: {} };
  this.defaults = registrationDefaults || {};
  this.library = {};

  // cellCriteria: A store of cells, their checks, and any specific data associated with each check.
  // cellCriteria = {
  //   'B2': [{ name: 'beginsWithEquals' },
  //          { name: 'containsSum'      }],
  //   'B3': [{ name: 'beginsWithEquals' },
  //          { name: 'containsSum'      },
  //          { name: 'hasXarguments'
  //            data: { argCount: 3 }    }]
  // }
  this.cellCriteria = {};

  // completionStatus: An object containing the completion status of all cells and their criteria.
  // completionStatus = {
  // 	'D4': {
  // 		allPassed: true,
  // 		checksPassed: {
  // 			beginsWithEquals: true,
  // 			containsSum: false
  // 		}]
  // 	}
  // }
  this.completionStatus = {};

  /**
   * Add a single check to the library. Assigns default values where values aren't provided.
   * If a check of this name already exists, it won't add it.
   *
   * @param  {Object} checkObj An object containing information about a check. Ex:
   *   {
   *     name: 'beginsWithEquals',
   *     regex: '^=',
   *     successAction: function(){},
   *     failureAction: function(){},
   *     data: {}
   *   },
   */
  this.addCheckToLibrary = function(checkObj) {
    var name = checkObj.name,
        regex = checkObj.hasOwnProperty('regex') ? checkObj.regex : this.defaults.regex,
        result = checkObj.hasOwnProperty('result') ? checkObj.result : this.defaults.result,
        test = checkObj.hasOwnProperty('test') ? checkObj.test : this.defaults.test || doNothing,
        exactRefs = checkObj.hasOwnProperty('exactRefs') ? checkObj.exactRefs : this.defaults.exactRefs || false,
        whitespaceSensitive = checkObj.hasOwnProperty('whitespaceSensitive') ? checkObj.whitespaceSensitive : this.defaults.whitespaceSensitive || false,
        negateRegex = checkObj.hasOwnProperty('negateRegex') ? checkObj.negateRegex : this.defaults.negateRegex || false,
        successAction = checkObj.hasOwnProperty('successAction') ? checkObj.successAction : this.defaults.successAction || doNothing,
        failureAction = checkObj.hasOwnProperty('failureAction') ? checkObj.failureAction : this.defaults.failureAction || doNothing,
        data = this.defaults.hasOwnProperty('data') ? miniClone(this.defaults.data) : {};

    // If there is already a check with this name, skip this one.
    if (this.library[name]) {
      return;
    }

    // Override default data on a per-property basis. Basically a 1-level object merge between defaults and args.
    if (checkObj.data) {
      for (var attrname in checkObj.data) { data[attrname] = checkObj.data[attrname]; }
    }

    // If a regex was provided, use it to build a little function for testing the precalculated input.
    // Or, if a result is provided, use it to build a little function for testing the calculated value.
    // If no regex or result is provided, that's fine, but then you must provide your own test function.
    if (regex) {
      test = function(prevalue, postvalue, data) {
        prevalue = prevalue.toString();

        // Remove dollar signs unless the "exact references" setting is set. This way the same check can work whether or
        // not the person used relative or absolute cell references.
        if (!exactRefs) {
          prevalue = prevalue.replace(/\$/g, '');
        }

        // Remove whitespace to make the check whitespace-insensitive.
        if (!whitespaceSensitive) {
          prevalue = prevalue.replace(/\s+/g, '');
        }

        return negateRegex ? !regex.test(prevalue) : regex.test(prevalue);
      };
    } else if (result) {
      test = function(prevalue, postvalue, data) {
        return (result.toUpperCase() === postvalue.toString().toUpperCase())
      }
    }

    this.library[name] = {
      regex: regex,
      test: test,
      successAction: successAction,
      failureAction: failureAction,
      data: data
    };
  };

  /**
   * A convenience function for adding an Array full of checks to the library at once.
   * @param {Array} - Array containing a batch of checks you want to add to the library.
   */
  this.addChecksToLibrary = function(checksArray) {
    for (var i = 0; i < checksArray.length; i++) {
      this.addCheckToLibrary(checksArray[i]);
    }
  };

  /**
   * A function for registering the passing criteria for a cell.
   * @param {String} - Cell reference. Ex. 'C3'
   * @param {Array}  - Array of checks required for a cell to pass. If the checks are
   *                   already in the library, then they only need to contain the name propery.
   *                   Ex:
   *  [
   * 		{ name: 'beginsWithEquals' },
   * 		{ name: 'containsSum' },
   * 		{
   * 			name: 'newCheck--endsInY',
   * 			regex: 'Y$'
   * 		}
   * 	]
   */
  this.registerCriteria = function(cellRef, checkList) {
    var checkName,
        checkData;

    this.cellCriteria[cellRef] = [];
    this.completionStatus[cellRef] = {
      allPassed: false,
      checksPassed: {}
    };

    for (var i = 0; i < checkList.length; i++) {
      checkName = checkList[i].name;
      checkData = checkList[i].data || undefined;

      // Add checks to the library if they aren't already there.
      if (!this.library[checkName]) {
        this.addCheckToLibrary(checkList[i]);
      }

      // Add to the cellCriteria object.
      this.cellCriteria[cellRef].push({
        name: checkName,
        data: checkData
      });

      // Add to the completionStatus object.
      this.completionStatus[cellRef].checksPassed[checkName] = false;
    }
  }

  /**
   * Given a cell reference, return a validation function that performs and tracks all the checks for that cell.
   * @param  {String} cellRef - The cell reference you want the validation function for.
   * @return {Function}       - A function for validating a cell, according to handsontable specs. It contains
   *                            everything needed to run all the checks for that cell, record results, and trigger
   *                            successActions, failureActions, and finalActions.
   */
  this.getValidationFunction = function(cellRef) {
    var thisChecker = this,
        batch = this.cellCriteria[cellRef];

    return function(prevalue, postvalue) {
      var passed,
          currentCheckName,
          currentCheckFromLibrary,
          currentCellCriteriaCheckData,
          currentCheckData,
          cellSettings = this;

      for (var i = 0; i < batch.length; i++) {
        currentCheckName = batch[i].name,
        currentCheckFromLibrary = thisChecker.library[currentCheckName],
        currentCellCriteriaCheckData = batch[i].data,
        currentCheckData = currentCheckFromLibrary.data;

        // If there's new data on this check in the cellCriteria, merge it with this check's library data.
        // @todo: make sure this merge isn't being saved to the check in the library (because of pass-by-reference).
        //        If it is, I should use miniClone instead.
        if (currentCellCriteriaCheckData) {
          for (var key in currentCellCriteriaCheckData) { currentCheckData[key] = currentCellCriteriaCheckData[key]; }
        }

        // Uncomment to debug:
        // console.log("detected prevalue");
        // console.log(prevalue);

        // console.log("detected postvalue");
        // console.log(postvalue);

        passed = currentCheckFromLibrary.test(prevalue, postvalue, currentCheckData);

        if (passed) {
          thisChecker.completionStatus[cellRef].checksPassed[currentCheckName] = true;
          currentCheckFromLibrary.successAction(currentCheckData, cellRef, this);
        } else {
          thisChecker.completionStatus[cellRef].checksPassed[currentCheckName] = false;
          thisChecker.completionStatus[cellRef].allPassed = false;
          currentCheckFromLibrary.failureAction(currentCheckData, cellRef, this);
          // Cut the loop short as soon as one fails.
          return false;
        }
      }

      // If we made it all the way here, then all the checks for the cell passed.
      thisChecker.completionStatus[cellRef].allPassed = true;

      if (thisChecker.everythingComplete()) {
        thisChecker.finalActionObj.callback(thisChecker.finalActionObj.data);
      }

      return true;
    };
  }

  /**
   * Check if all checks have been completed on all cells.
   * @return {Boolean} - Returns true if everything is completed.
   */
  this.everythingComplete = function() {
    var allCells = this.completionStatus;
    for (var cell in allCells) {
      if (allCells.hasOwnProperty(cell)) {
        if (!allCells[cell].allPassed) {
          return false;
        }
      }
    }

    return true;
  };

  /**
   * A tiny deep-clone function, with caveauts (object can't contain dates, functions, or undefined).
   * See http://stackoverflow.com/a/4591639/1154642
   * @param  {Object} object - The object you want to clone.
   * @return {Object}        - The cloned copy of the original object.
   */
  function miniClone(object) {
    return JSON.parse(JSON.stringify(object));
  }
}
