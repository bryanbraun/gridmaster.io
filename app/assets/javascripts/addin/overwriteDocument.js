GM.overwriteDocument = (function() {
  /**
   * Checks whether a range object's address is A1.
   * Works for addresses with or without the sheet name.
   * @param {Object} usedRange
   */
  function isRangeA1(usedRange) {
    return usedRange.address === "A1" || usedRange.address.split("!")[1] === "A1";
  }

  /**
   * Check the existing workbook for user content and prompt for permission to overwrite it.
   * Once granted, it gives blanket permission to overwrite content in this workbook from now on.
   * Permission is assumed if the workbook is empty to begin with.
   *
   * Note 1: there might be a way to batch these checks with the addOrRemoveWorksheets tasks for
   *         performance reasons, but given that this is a one-time thing, I think it's worth the
   *         small hit to keep the code separate and clean.
   * Note 2: We like using callbacks because it makes it easy to conditionally continue or not.
   *         We once refactored to promises only, but backed ot of it because this was less straightforward.
   */
  function checkSpreadsheetForUserContentAsync(callback) {
    var isOverwriteApproved = Office.context.document.settings.get("OverwriteApproved");
    if (isOverwriteApproved) {
      callback();
      return;
    }

    return Excel.run(function(ctx) {
      var sheets = ctx.workbook.worksheets;
      sheets.load("items/name");

      return ctx.sync().then(function() {
        var worksheetUsedRanges = [];

        sheets.items.forEach(function(sheet) {
          worksheetUsedRanges.push(sheet.getUsedRange().load("address"));
        });

        return ctx.sync().then(function() {
          var isWorkbookEmpty = worksheetUsedRanges.every(isRangeA1);
          if (!isWorkbookEmpty) {
            showOverwriteWarning();
          } else {
            onApproveOverwrite(callback);
          }
        });
      });
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  function showOverwriteWarning() {
    $("#overwriteDocument").show();
  }

  function onApproveOverwrite(callback) {
    // We use 1 = true and 0 = false, when setting values because the API doesn't take booleans.
    Office.context.document.settings.set("OverwriteApproved", 1);
    Office.context.document.settings.saveAsync(ExcelUtils.logAnyAsyncErrors);
    $("#overwriteDocument").hide();
    callback();
  }

  function onDeclineOverwrite() {
    $("#overwrite-question").addClass("is-hidden");
    $("#overwrite-declined").removeClass("is-hidden");
  }

  return {
    checkSpreadsheetForUserContentAsync: checkSpreadsheetForUserContentAsync,
    onApproveOverwrite: onApproveOverwrite,
    onDeclineOverwrite: onDeclineOverwrite
  };
})();
