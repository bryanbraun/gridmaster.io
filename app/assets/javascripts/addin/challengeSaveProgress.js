GM.challengeSaveProgress = (function() {

  /**
   * When we submit a login or signup request from the challenges page,
   * it triggers a page reload, so we ought to cleanup artifacts first.
   *
   * If we don't do this, we can get weird bugs. Like if we load a challenge
   * page, and there are existing bindings, but it cleans and re-adds
   * the SAME bindings, Excel will *say* that the handlers get added,
   * but cell changes won't be responsive. It's weird.
   *
   * @param {Event} event
   */
  function cleanupChallengeArtifactsBeforeSubmittingLoginForm(event) {
    event.preventDefault();
    var $form = $(this);

    hideSaveYourProgress();
    GM.challenges.cleanupAnyChallengeArtifacts(function() {
      $form.off("submit").submit();
    }, true);
  }

  /**
   * Same as the function above except it's for the Signup Form. It has minor differences
   * because it's triggered by a recaptcha callback instead of an onSubmit event
   * (we only use recaptcha on sign up).
   */
  function cleanupChallengeArtifactsBeforeSubmittingSignupForm() {
    hideSaveYourProgress();
    GM.challenges.cleanupAnyChallengeArtifacts(function() {
      document.getElementById("sign-up-and-save-progress").submit();
    }, true);
  }

  function onContinueWithoutSaving() {
    GM.challengeSaveProgress.suppressSaveYourProgressPrompt = true;

    hideSaveYourProgress();
    GM.challenges.updateStep(GM.challenges.getCurrentStep() + 1);
  }

  function showSaveYourProgress() {
    $("#save-your-progress").show();
  }

  function hideSaveYourProgress() {
    $("#save-your-progress").hide();
  }

  function init() {
    $("#log-in-and-save-progress").on("submit", cleanupChallengeArtifactsBeforeSubmittingLoginForm);

    $("#prompt-signup-link").click(function(e) {
      e.preventDefault();
      $("#login").addClass("is-hidden");
      $("#signup").removeClass("is-hidden");
    });

    $("#prompt-login-link").click(function(e) {
      e.preventDefault();
      $("#signup").addClass("is-hidden");
      $("#login").removeClass("is-hidden");
    });
  }

  return {
    init: init,
    // The suppressSaveYourProgressPrompt value is stored in memory, meaning, that it's set
    // until you reload the page. Thus, it won't show the prompt for this lesson's remaining challenge
    // steps, but will show the prompt again when you complete a step in the next lesson's challenge.
    suppressSaveYourProgressPrompt: false,
    onContinueWithoutSaving: onContinueWithoutSaving,
    cleanupChallengeArtifactsBeforeSubmittingSignupForm: cleanupChallengeArtifactsBeforeSubmittingSignupForm,
    showSaveYourProgress: showSaveYourProgress,
    hideSaveYourProgress: hideSaveYourProgress,
  };
})();

// I don't need to wait until Office.js is ready to run this... I only need the DOM
// to be ready, so I'll run it on $(document).ready. This also has the benefit of
// having it working in non-office environments for navigation and debugging purposes.
$(document).ready(GM.challengeSaveProgress.init);
