GM.conclusion = (function() {
  function isConclusionPage() {
    return !!document.querySelector(".conclusion");
  }

  function triggerConfetti() {
    var confettiSource = document.querySelector(".conclusion__confetti");
    window.domConfetti.confetti(confettiSource, {
      spread: 60,
      startVelocity: 35
    });
  }

  function populateCertificate(e) {
    e.preventDefault();
    var userName = $("#certificate-data").data("user-name");
    var courseName = $("#certificate-data").data("course-name");

    $.getJSON("/data/course-certificate.json", function(worksheetsData) {
      var stepData = { worksheets: worksheetsData };

      // Populate dynamic data into specific hard-coded cells in our data.
      worksheetsData[0]["tableData"][17][4] = userName;
      worksheetsData[0]["tableData"][23][5] = courseName;
      ExcelUtils.addOrRemoveWorksheetsAsync(stepData).then(function() {
        return ExcelUtils.updateWorksheetsAsync(worksheetsData);
      });
    });
  }

  function init() {
    if (isConclusionPage()) {
      triggerConfetti();
      document.querySelector(".certificate-link").addEventListener("click", populateCertificate);
    }
  }

  return {
    init: init
  };
})();

GM.initializer.register(GM.conclusion.init);
