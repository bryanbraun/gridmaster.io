// Modal - Custom JS to power the Office Fabric modal component:
// https://dev.office.com/fabric#/components/modal
//
// Assumptions:
//  - The modal markup should already be on the page somewhere.
//  - Modals can only be closed by clicking the "x" icon on the modal header
//    (which requires a class of `class="modal-dismiss"`).
//  - You can trigger the modal by adding a link to the page with a
//    `data-modal-trigger-id="my-modal-id"` (which will show the modal when clicked)
//  - If you want to trigger show or hide a modal directly, just use jquery:
//    `$('#my-modal-id').show()` or `$('#my-modal-id').hide()`
GM.modal = (function() {

  function triggerModal(e) {
    e.preventDefault();
    var triggerID = $(this).data('modal-trigger-id');
    $('#' + triggerID).show();
  }

  function dismissModal(e) {
    e.preventDefault();
    $(this).closest('.modal').hide();
  }

  function init() {
    $('[data-modal-trigger-id]').click(triggerModal);
    $('.modal__dismiss').click(dismissModal);
  }

  return {
    init: init
  }
})();

// I don't need to wait until Office.js is ready to run this... I only need the DOM
// to be ready, so I'll run it on $(document).ready. This also has the benefit of
// having it working in non-office environments for navigation and debugging purposes.
$(document).ready(GM.modal.init);
