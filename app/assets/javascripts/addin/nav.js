/**
 * Javascript for interactive nav behaviors
 * @return {Object} - Public methods.
 */
GM.nav = (function() {
  var body = document.querySelector('body');
      $menuButton = $('.addin-settings');
      $fakeLogoutLink = $('.fake-logout-link');

  /**
   * Allows a fake logout link to trigger the real logout link. Why use a fake logout link?
   * We sometimes need to clean up Excel artifacts on logout (like when we are logging out from
   * a challenge page). Unfortunately, rails' logout links have "method: :delete" on them, and
   * we can't add cleanup listeners on them because Rails already has it's own listeners on them
   * for submitting the request (and we can't ensure that ours would run before theirs). This is
   * the solution.
   * @param {Event} event
   */
  function triggerLogout(event) {
    event.preventDefault;

    if (GM.challenges.isChallengePage()) {
      GM.challenges.clearAddinChallengeContent();
      GM.challenges.cleanupAnyChallengeArtifacts(function() {
        $(".real-logout-link").trigger("click");
      }, true);
    } else {
      $(".real-logout-link").trigger("click");
    }
  }

  function openMenu(event) {
    event.preventDefault;
    $(this).toggleClass("is-open");
  }

  /**
   * Uses event delegation to close the menu when clicking on anything other than a menu item.
   * @param {Event} e
   */
  function closeMenu(e) {
    if (!$(e.target).is(".addin-settings, .addin-settings > *") && $menuButton.hasClass("is-open")) {
      $menuButton.removeClass("is-open");
    }
  }

  function init() {
    $menuButton.click(openMenu);

    body.addEventListener("click", closeMenu);

    $fakeLogoutLink.click(triggerLogout);
  }

  return {
    init: init
  }
})();

// I don't need to wait until Office.js is ready to run this... I only need the DOM
// to be ready, so I'll run it on $(document).ready. This also has the benefit of
// having it working in non-office environments for navigation and debugging purposes.
$(document).ready(GM.nav.init);
