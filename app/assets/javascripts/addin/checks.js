/**
 * An array of generic checks for loading into the exercise checker.
 *
 * Notes:
 * Maybe this can be part of the "Exercise Checker" Repo?
 *
 * We store Regex literals here instead of the "stringified" versions we have to store in the JSON.
 * The only differences are:
 * 	1. We wrap the regex in '/' and '/i' instead of quotes.
 * 	2. We do not need to double escape characters (the JSON has '\\' instead of '\').
 *
 * Checks are by default:
 * 	- Case insensitive (when denoted with the 'i' flag)
 *  - Whitespace insensitve
 *  	- This is done by stripping out spaces, so don't include spaces in your regex rules.
 *    - This can be overridden by adding whitespaceSensitive: true, to your check.
 *  - Exact Reference insensitive (cell references can be absolute or relative)
 *  	- This is done by stripping out '$' (so don't include '$' in your regex rules).
 *  	- This can be overridden by adding exactRefs: true, to your check.
 *
 * Full Example Check:
 * {
 *   name: 'containsEquals',                          // Check name. Each one in the library should be unique.
 *                                                    //   default: undefined
 *   regex: /=/,                                      // Test: A regular expression, used to match against the value entered in the cell.
 *                                                    //   default: undefined
 *   result: '25'                                     // Alternative test: compares this value to the calculated value to see if equal. Whitespace sensitive & case insensitive by default. Repeating decimals repeat 14 places.
 *                                                    //   default: undefined
 * 	 test: function(prevalue, postvalue, data) {...}, // An alternative test function to run in place of regex or result.
 * 	                                                  //   default: undefined
 * 	 exactRefs: false,                                // Setting to determine if cell references must exactly match absoluteness / relativeness. (B3 !== $B$3)
 * 	                                                  //   default: false
 *   whitespaceSensitive false,                       // Makes the regex check whitespace senstive, if true
 *                                                    //   default: false
 * 	 negateRegex: false,                               // Negates the regex, if true. Example: /=/ would pass if there's NOT an equals in the cell.
 * 	                                                  //   default: false
 * 	 successAction: function(data, cellRef) {...},    // Callback to run if the check passes successfully.
 * 	                                                  //   default: function(){}
 * 	 failureAction: function(data, cellRef) {...},    // Callback to run if the check fails.
 * 	                                                  //   default: function(){}
 * 	 data: {...}                                      // Custom data to pass into callback functions like test, successAction, or failureAction.
 * 	                                                  //   default: {}
 * }
 */
GM.checks = [
  {
    name: "beginsWithEquals",
    regex: /^=/,
    data: {
      mistakeMessage: "The formula should begin with '='."
    }
  },
  {
    name: "doesntBeginWithEquals",
    regex: /^=/,
    negateRegex: true,
    data: {
      mistakeMessage: "The contents of this cell shouldn't begin with '='."
    }
  },
  {
    name: "containsPlus",
    regex: /\+/,
    data: {
      mistakeMessage: "The formula should contain a '+'."
    }
  },
  {
    name: "containsMinus",
    regex: /\-/,
    data: {
      mistakeMessage: "Hmm, the formula should contain a '-'."
    }
  },
  {
    name: "containsAsterisk",
    regex: /\*/,
    data: {
      mistakeMessage: "It looks like this formula should contain a '*'."
    }
  },
  {
    name: "containsDivision",
    regex: /\//,
    data: {
      mistakeMessage: "It looks like this formula should contain a '/'."
    }
  },
  {
    name: "contains?",
    regex: /\?/,
    data: {
      mistakeMessage: "Hold up, this formula should contain a '?'."
    }
  },
  {
    name: "containsExponent",
    regex: /\^/,
    data: {
      mistakeMessage: "Hold up, this formula should contain a '^'."
    }
  },
  {
    // Note: this will not match '>='
    name: "containsGT",
    regex: />(?!=)/,
    data: {
      mistakeMessage: "This formula should include a '>' operator."
    }
  },
  {
    name: "containsGTE",
    regex: />=/,
    data: {
      mistakeMessage: "This formula should include a '>=' operator."
    }
  },
  {
    // Note: this will not match '<='
    name: "containsLT",
    regex: /<(?!=)/,
    data: {
      mistakeMessage: "This formula should include a '<' operator."
    }
  },
  {
    name: "containsLTE",
    regex: /<=/,
    data: {
      mistakeMessage: "This formula should include a '<=' operator."
    }
  },
  {
    name: "containsNotEqualTo",
    regex: /<>/,
    data: {
      mistakeMessage: "This formula should include a '<>' operator."
    }
  },
  {
    name: "containsSUM",
    regex: /SUM\(/i,
    data: {
      mistakeMessage: "We want to be using the SUM function here."
    }
  },
  {
    name: "containsAVERAGE",
    regex: /AVERAGE\(/i,
    data: {
      mistakeMessage: "We want to be using the AVERAGE function here."
    }
  },
  {
    name: "containsROUND",
    regex: /ROUND\(/i,
    data: {
      mistakeMessage: "We want to be using the ROUND function here."
    }
  },
  {
    name: "containsCOUNT",
    regex: /COUNT\(/i,
    data: {
      mistakeMessage: "We want to be using the COUNT function here."
    }
  },
  {
    name: "containsIF",
    regex: /IF\(/i,
    data: {
      mistakeMessage: "We want to be using the IF function here."
    }
  },
  {
    name: "containsIFERROR",
    regex: /IFERROR\(/i,
    data: {
      mistakeMessage: "We want to be using the IFERROR function here."
    }
  },
  {
    name: "containsCOUNTIF",
    regex: /COUNTIF\(/i,
    data: {
      mistakeMessage: "We want to be using the COUNTIF function here."
    }
  },
  {
    name: "containsSUMIF",
    regex: /SUMIF\(/i,
    data: {
      mistakeMessage: "We want to be using the SUMIF function here."
    }
  },
  {
    name: "containsAVERAGEIF",
    regex: /AVERAGEIF\(/i,
    data: {
      mistakeMessage: "We want to be using the AVERAGEIF function here."
    }
  },
  {
    name: "containsAVERAGEIFS",
    regex: /AVERAGEIFS\(/i,
    data: {
      mistakeMessage: "We want to be using the AVERAGEIFS function here."
    }
  },
  {
    name: "containsDATEVALUE",
    regex: /DATEVALUE\(/i,
    data: {
      mistakeMessage: "We want to be using the DATEVALUE function here."
    }
  },
  {
    name: "containsTODAY",
    regex: /TODAY\(/i,
    data: {
      mistakeMessage: "We want to be using the TODAY function here."
    }
  },
  {
    name: "containsISBLANK",
    regex: /ISBLANK\(/i,
    data: {
      mistakeMessage: "We want to be using the ISBLANK function here."
    }
  },
  {
    name: "containsEXACT",
    regex: /EXACT\(/i,
    data: {
      mistakeMessage: "We want to be using the EXACT function here."
    }
  },
  {
    name: "containsNOT",
    regex: /NOT\(/i,
    data: {
      mistakeMessage: "We want to be using the NOT function here."
    }
  },
  {
    name: "containsAND",
    regex: /AND\(/i,
    data: {
      mistakeMessage: "We want to be using the AND function here."
    }
  },
  {
    name: "containsOR",
    regex: /OR\(/i,
    data: {
      mistakeMessage: "We want to be using the OR function here."
    }
  },
  {
    name: "containsVLOOKUP",
    regex: /VLOOKUP\(/i,
    data: {
      mistakeMessage: "We want to be using the VLOOKUP function here."
    }
  },
  {
    name: "isOnlyLetters",
    regex: /[\W\s\d]/i,
    negateRegex: true,
    data: {
      mistakeMessage:
        "We're expecting to only see letters in this cell (no numbers, operators, or functions)."
    }
  },
  {
    name: "isNumeric",
    // See https://stackoverflow.com/a/1830844/1154642
    test: function(prevalue, postvalue, data) {
      return !isNaN(parseFloat(prevalue)) && isFinite(prevalue);
    },
    data: {
      mistakeMessage: "We're expecting a number in this cell."
    }
  },
  {
    name: "isBlank",
    regex: /^$/,
    whitespaceSensitive: true,
    data: {
      mistakeMessage: "We're expecting this cell to be blank."
    }
  },
  {
    name: "usesCellRefs",
    // This regex matches both relative & absolute references.
    regex: /\$?[a-z]+\$?\d+/i,
    data: {
      mistakeMessage: "You need to use cell references in this formula."
    }
  },
  {
    // This check fails when a cell reference is included. Works for both relative & absolute references.
    // The message implies you *could* do the task with cell references. That's fine for now, because
    // it's currently true for all situations where I'm using this check.
    name: "doesntUseCellRefs",
    regex: /\$?[a-z]+\$?\d+/i,
    negateRegex: true,
    data: {
      mistakeMessage:
        "Hold up! We'd like to complete this task without using cell references, for now."
    }
  },
  {
    name: "containsARange",
    regex: /:/,
    data: {
      mistakeMessage: 'You need to reference a range of cells (using a ":").'
    }
  },
  {
    // The message implies you *could* do the task with a range. That's fine for now, because
    // it's currently true for all situations where I'm using this check.
    name: "doesntContainARange",
    regex: /:/,
    negateRegex: true,
    data: {
      mistakeMessage:
        'Actually, for now, let\'s try and complete this task without using a range (":").'
    }
  },
  {
    // we use custom functions for 'resultIsTRUE' and 'resultIsFALSE' so we don't have to
    // worry about issues around falsy checks or string conversion in the "result:" option.
    name: "resultIsTRUE",
    test: function(prevalue, postvalue, data) {
      return postvalue === true;
    },
    data: {
      mistakeMessage:
        "Hmm, something seems off. We're expecting this formula to result in TRUE."
    }
  },
  {
    name: "resultIsFALSE",
    test: function(prevalue, postvalue, data) {
      return postvalue === false;
    },
    data: {
      mistakeMessage:
        "Hmm, something seems off. We're expecting this formula to result in FALSE."
    }
  },
  {
    name: "contains_N_IFs",
    test: function(prevalue, postvalue, data) {
      var found = prevalue.match(/IF\(/gi);
      return found.length === data.numberOfIfs;
    },
    data: {
      numberOfIfs: 2,
      mistakeMessage: "This formula should include two IF statements."
    }
  },
  {
    // These count the total arguments in the whole formula. It only really works best for single-function formulas.
    name: "tooManyArguments",
    test: function(prevalue, postvalue, data) {
      var found = prevalue.match(/\,/g),
        argCount = Array.isArray(found) ? found.length + 1 : 0;

      if (argCount > data.threshold) {
        return false;
      }
      return true;
    },
    data: {
      threshold: 3, // the expected number of arguments
      mistakeMessage:
        "Hmm. You're using more arguments in your function than we're expecting."
    }
  },
  {
    name: "notEnoughArguments",
    test: function(prevalue, postvalue, data) {
      var found = prevalue.match(/\,/g),
        argCount = Array.isArray(found) ? found.length + 1 : 0;

      if (argCount < data.threshold) {
        return false;
      }
      return true;
    },
    data: {
      threshold: 3,
      mistakeMessage:
        "Hmm. You're using fewer arguments in your function than we're expecting."
    }
  },
  {
    name: "allRefsAreInTheSameColumn",
    test: function(prevalue, postvalue, data) {
      var commonColumn,
        refsArray = prevalue.match(/\$?[a-z]+\$?\d+/gi);

      commonColumn = refsArray
        .map(function(reference) {
          return reference.replace(/\d+/g, "");
        })
        .reduce(function(a, b) {
          return a === b ? a : NaN;
        });

      // If there's no commonColumn, this value is NaN, and !!NaN will return false.
      return !!commonColumn;
    },
    data: {
      mistakeMessage:
        "Hmm. We're expecting all our cell references to be in the same column."
    }
  },
  {
    // A special test for cells with empty strings. Use this, instead of a check with `result: ""`
    name: "resultIsEmptyString",
    test: function(prevalue, postvalue, data) {
      return postvalue === "";
    },
    data: {
      mistakeMessage:
        "Oops. We're expecting this cell to return an empty string."
    }
  }
];
