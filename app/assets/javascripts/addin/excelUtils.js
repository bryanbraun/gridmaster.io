/**
 * A collection of functions for interacting with the underlying Excel Document.
 * They are gridmaster-agnostic, meaning, they could be useful to anyone making
 * an Excel addin.
 *
 * This includes
 *  - Functions that smooth out rough patches in the excel API.
 *  - Excel-related utilities.
 *
 * Notes:
 *   - Asynchronous functions have 'async' in the function name.
 *   - There's a dependency on jQuery that's probably not going away anytime soon.
 *     (deferred's are non-trivial to replace with something else).
 */

ExcelUtils = (function() {
  var batchedHighlights = [];
  var debouncedRunBatchedHighlights = debounce(runBatchedHighlights, 150);

  /**
   * Builds a batch of cell highlights, to be executed on a debounce schedule. The data looks like this:
   *  [
   *    {
   *      name: "Sheet1",
   *      batch: [{
   *        range: A1,
   *        action: "clearFill"
   *      }]
   *    },
   *    {
   *      name: "Sheet2",
   *      batch: [{
   *        range: A1,
   *        action: "clearFill"
   *      }]
   *    }
   *  ]
   *
   * We use this structure so we can loop through the spreadsheets in our runBatchedHighlights
   * task and get them all applied in one sync().
   *
   * @param {*} fullCellRef
   * @param {*} hexColor
   */
  function highlightCellsBatched(fullCellRef, hexColor) {
    var cellRefParts = fullCellRef.split("!");
    var worksheetName = cellRefParts[0];
    var cellRef = cellRefParts[1];
    var worksheetIndex = undefined;

    batchedHighlights.forEach(function(batchedWorksheet, index) {
      if (batchedWorksheet.name === worksheetName) {
        worksheetIndex = index;
      }
    });

    if (worksheetIndex === undefined) {
      // ".push()" returns array length, therefore, ".push() - 1" returns array index.
      worksheetIndex =
        batchedHighlights.push({
          name: worksheetName,
          batch: []
        }) - 1;
    }

    if (hexColor === "") {
      batchedHighlights[worksheetIndex].batch.push({
        range: cellRef,
        action: "clearFill"
      });
    } else {
      batchedHighlights[worksheetIndex].batch.push({
        range: cellRef,
        action: "set",
        options: {
          property: "format.fill.color",
          value: hexColor
        }
      });
    }

    debouncedRunBatchedHighlights();
  }

  function runBatchedHighlights() {
    // We remove contents of the batch we are running here, to prevent any
    // accidental duplicate runs. Uses this technique: http://stackoverflow.com/a/4591639/1154642.
    var clonedBatchedHighlights = JSON.parse(JSON.stringify(batchedHighlights));
    batchedHighlights = [];

    Excel.run(function(ctx) {
      var worksheet;
      for (var i = 0; i < clonedBatchedHighlights.length; i++) {
        worksheet = ctx.workbook.worksheets.getItem(clonedBatchedHighlights[i].name);
        formatWorksheet(worksheet, clonedBatchedHighlights[i].batch);
      }
      return ctx.sync();
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  /**
   * Clear out the cell highlights for one or more cell references.
   * @param {String|Array} refs
   * @param {Function} callback
   */
  function clearHighlights(refs, callback) {
    var refsArray = [].concat(refs); // Make refs into an array, if it isn't one already.
    Excel.run(function(ctx) {
      var worksheetName, worksheet, cellRefParts, cellRef;

      for (var i = 0; i < refsArray.length; i++) {
        cellRefParts = refsArray[i].split("!");
        worksheetName = cellRefParts[0];
        cellRef = cellRefParts[1];
        worksheet = ctx.workbook.worksheets.getItem(worksheetName);
        worksheet.getRange(cellRef).format.fill.clear();
      }
      return ctx.sync().then(callback);
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  /**
   * Highlight cells for one or more cell references.
   * @param {String|Array} cellRefs
   * @param {String} hexColor
   * @param {Function} callback
   */
  function highlightCells(cellRefs, hexColor, callback) {
    var refsArray = [].concat(cellRefs); // Make refs into an array, if it isn't one already.
    Excel.run(function(ctx) {
      var worksheet = ctx.workbook.worksheets.getActiveWorksheet();
      for (var i = 0; i < refsArray.length; i++) {
        worksheet.getRange(refsArray[i]).format.fill.color = hexColor;
      }
      return ctx.sync().then(callback);
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  /**
   * Gives a cell range (ex: A1:AA9) for a JS data matrix
   * Currently assumes:
   *   - an origin of A1
   *   - all rows are equal length
   *
   * @param    {Array} - An array of arrays, representing excel data.
   * @return   {String} - A string representing the Excel range the data spans.
   */
  function getRangeForData(matrix) {
    var numberOfRows = matrix.length,
      numberOfCols = matrix[0].length,
      dividend = numberOfCols,
      columnName = "",
      modulus;

    while (dividend > 0) {
      modulus = dividend % 26;
      columnName = String.fromCharCode(64 + modulus) + columnName;
      dividend = (dividend - modulus) / 26;
    }

    return "A1:" + columnName + numberOfRows;
  }

  /**
   * Gets the value from a matrix, corresponding to an Excel cell reference.
   * @param {String} cellRef - A cell reference for a single cell. Eg: 'B5'
   * @param {Array} matrix - An array of arrays, representing spreadsheet data
   */
  function getCellRefValueFromMatrix(cellRef, matrix) {
    var letterRegex = /[a-z]+/gi;
    var numberRegex = /\d+/gi;
    var colRefMatch = cellRef.match(letterRegex);
    var rowRefMatch = cellRef.match(numberRegex);
    var colRefNumber = 0;
    var refValue;

    if (colRefMatch.length !== 1 || rowRefMatch.length !== 1) {
      return; // Cell ref is invalid
    }

    var colRefLetter = colRefMatch[0];
    var rowRefNumber = parseInt(rowRefMatch[0], 10);

    for (var letterIndex = 0; letterIndex < colRefLetter.length; letterIndex++) {
      colRefNumber *= 26;
      colRefNumber += colRefLetter.charCodeAt(letterIndex) - 64;
    }

    // We -1 because the arrays are 0-indexed.
    refValue = matrix && matrix[rowRefNumber - 1] && matrix[rowRefNumber - 1][colRefNumber - 1];

    return refValue || "";
  }

  /**
   * Excel references with spaces in the worksheet name should be quoted in order to
   * use them in some API interactions (like setting bindings). This function does that.
   *
   * Example:
   *   "Food Trucks1!A2" => "'Food Trucks'1!A2"
   *
   * @param {string} reference (like "Food Trucks1!A2" or "Data!A3:B9" )
   * @return {string}
   */
  function quoteReferenceIfNecessary(reference) {
    var isSpaceInName = reference.indexOf(" ") >= 0;
    var isFullCellRef = reference.indexOf("!") >= 0;

    if (!isSpaceInName || !isFullCellRef) {
      return reference;
    }

    var referenceParts = reference.split("!");
    return "'" + referenceParts[0] + "'!" + referenceParts[1];
  }

  /**
   * Catches and logs any accumulated errors that bubble up from the Excel.run execution.
   * @param {Object} error - The error object.
   */
  function logExcelRunErrors(error) {
    console.log("Error: " + error);
    console.log(error);
    if (error instanceof OfficeExtension.Error) {
      console.log("Debug info: " + JSON.stringify(error.debugInfo));
    }
  }

  /**
   * A function for printing all bindings to the console. Created for debugging purposes.
   */
  function logAnyAsyncErrors(asyncResult) {
    if (asyncResult.status == Office.AsyncResultStatus.Failed) {
      console.log("Error: " + asyncResult.error.message);
      console.log(error);
    }
  }

  /**
   * A function for printing all bindings to the console. Created for debugging purposes.
   */
  function logAllBindings() {
    Office.context.document.bindings.getAllAsync(function(asyncResult) {
      if (asyncResult.status == Office.AsyncResultStatus.Failed) {
        console.log("Error: " + asyncResult.error.message);
      } else {
        console.log("List of bindings: ");
        console.log(asyncResult.value);
      }
    });
  }

  /**
   * Smartly determine from the data which worksheet should be active. If there's more than one
   * worksheet, we activate the first one with the {active: true} flag in the data. If the flag
   * doesn't exist, we activate the the first one with checkData.
   *
   * Note: This is easily testable. This function could be a starting place for writing tests.
   * @param {Array} worksheetsData - An array of worksheet data
   * @return {Number}              - The index of the worksheet we want to make active.
   */
  function getActiveSheetIndex(worksheetsData) {
    var sheetIndexToMakeActive = 0; // default value

    if (worksheetsData.length > 1) {
      // These would be more succinct with .findIndex, but browser support is meh.
      var worksheetsWithActiveFlag = worksheetsData.map(function(worksheet) {
        return !!worksheet.active;
      });
      var worksheetsWithCheckData = worksheetsData.map(function(worksheet) {
        return !!worksheet.checkData;
      });

      var firstPositionOfActiveFlag = worksheetsWithActiveFlag.indexOf(true);
      var firstPositionOfCheckData = worksheetsWithCheckData.indexOf(true);

      if (firstPositionOfActiveFlag !== -1) {
        sheetIndexToMakeActive = firstPositionOfActiveFlag;
      } else if (firstPositionOfCheckData !== -1) {
        sheetIndexToMakeActive = firstPositionOfCheckData;
      }
    }

    return sheetIndexToMakeActive;
  }

  /**
   * Replaces any existing worksheets with the new ones defined in the stepData
   * @param {Array} stepData An array of all the data for a given challenge step.
   */
  function addOrRemoveWorksheetsAsync(stepData) {
    return Excel.run(function(ctx) {
      var existingWorksheets = ctx.workbook.worksheets;
      existingWorksheets.load("items/name");

      return ctx.sync().then(function() {
        var existingWorksheetNames = existingWorksheets.items.map(function(worksheet) {
          return worksheet.name;
        });
        var futureWorksheetNames = stepData.worksheets.map(function(worksheet) {
          return worksheet.name;
        });
        var worksheetsToCreate = futureWorksheetNames.filter(function(futureWorksheetName) {
          return existingWorksheetNames.indexOf(futureWorksheetName) < 0;
        });
        var worksheetsToDelete = existingWorksheets.items.filter(function(existingWorksheet) {
          return futureWorksheetNames.indexOf(existingWorksheet.name) < 0;
        });

        if (worksheetsToCreate.length === 0 && worksheetsToDelete.length === 0) {
          // no worksheet changes needed. exiting early
          return;
        }

        // We create new worksheets before we delete old ones so we never end up in a
        // situation where a workbook has no worksheets, (which would result in an error).
        worksheetsToCreate.forEach(function(worksheetName) {
          existingWorksheets.add(worksheetName);
        });
        worksheetsToDelete.forEach(function(worksheetObject) {
          worksheetObject.delete();
        });

        return ctx.sync();
      });
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  /**
   * Clears out the old worksheet data/formats and adds the new data/formats
   * @param    {Array}    worksheetsData An array of worksheet data (including cell values and formats)
   * @param    {ANY}      args      Any arguments you want to pass into the callback function
   * @param    {Function} optionalCallback A function to call once the worksheets are updated.
   */
  function updateWorksheetsAsync(worksheetsData, args, optionalCallback) {
    var callback = optionalCallback || function() {};
    var targetCellColor = "#deecf9";
    var activeSheetIndex = ExcelUtils.getActiveSheetIndex(worksheetsData);

    return Excel.run(function(ctx) {
      var worksheets = ctx.workbook.worksheets;

      worksheetsData.forEach(function(worksheetData, index) {
        var worksheet = worksheets.getItem(worksheetData.name);
        var rangeName = ExcelUtils.getRangeForData(worksheetData.tableData);
        var cellsWithChecks;
        var range;

        // Select the active sheet and scroll it to the top left.
        if (index === activeSheetIndex) {
          worksheet.activate();
          worksheet.getRange("A1").select();
        }

        // Clear out old data.
        worksheet.getUsedRange().clear();

        // Add in new data
        range = worksheet.getRange(rangeName);
        range.values = worksheetData.tableData;

        worksheet
          .getUsedRange()
          .getEntireColumn()
          .format.autofitColumns();
        worksheet
          .getUsedRange()
          .getEntireRow()
          .format.autofitRows();

        // Manually format the spreadsheet
        if (worksheetData.tableSettings) {
          ExcelUtils.formatWorksheet(worksheet, worksheetData.tableSettings);
        }

        // Highlight cells with checks
        if (worksheetData.checkData) {
          cellsWithChecks = Object.keys(worksheetData.checkData);

          for (var i = 0; i < cellsWithChecks.length; i++) {
            worksheet.getRange(cellsWithChecks[i]).format.fill.color = targetCellColor;
          }
        }
      });

      return ctx.sync().then(function onWorksheetUpdated() {
        callback(args);
      });
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  /**
   * Format a worksheet, via the settings defined in a data object.
   * @param {Object} worksheet - an Excel worksheet object.
   *                             See https://dev.office.com/reference/add-ins/excel/worksheet?product=excel.
   * @param {Array} settings - an array of settings to be applied sequentially to ranges in the worksheet
   */
  function formatWorksheet(worksheet, settings) {
    // Defines a utility function similar to lodash's _set().
    // See https://stackoverflow.com/a/13719799/1154642.
    function assign(obj, prop, value) {
      if (typeof prop === "string") {
        prop = prop.split(".");
      }

      if (prop.length > 1) {
        var e = prop.shift();
        obj[e] = Object.prototype.toString.call(obj[e]) === "[object Object]" ? obj[e] : {};
        assign(obj[e], prop, value);
      } else {
        obj[prop[0]] = value;
      }
    }

    var methods = {
      set: function(range, options) {
        var property = options.property; // example: "format.font.color"
        var value = options.value; // example: "red"

        assign(range, property, value);
      },
      merge: function(range, options) {
        range.merge();
      },
      border: function(range, options) {
        // See https://dev.office.com/reference/add-ins/excel/rangeborder
        var rangeBorderObj = range.format.borders.getItem(options.position);
        var optionsArray = Object.keys(options).filter(function(item) {
          // we remove "position" from our optionsArray because we've
          // already used it to get the rangeBorderObj.
          return item !== "position";
        });
        optionsArray.forEach(function(key) {
          rangeBorderObj[key] = options[key];
        });
      },
      clearFill: function(range) {
        range.format.fill.clear();
      }
    };

    for (var i = 0; i < settings.length; i++) {
      var range = worksheet.getRange(settings[i].range);

      methods[settings[i].action](range, settings[i].options);
    }
  }

  return {
    clearHighlights: clearHighlights,
    highlightCells: highlightCells,
    highlightCellsBatched: highlightCellsBatched,
    getRangeForData: getRangeForData,
    logExcelRunErrors: logExcelRunErrors,
    logAnyAsyncErrors: logAnyAsyncErrors,
    logAllBindings: logAllBindings,
    formatWorksheet: formatWorksheet,
    quoteReferenceIfNecessary: quoteReferenceIfNecessary,
    getCellRefValueFromMatrix: getCellRefValueFromMatrix,
    getActiveSheetIndex: getActiveSheetIndex,
    updateWorksheetsAsync: updateWorksheetsAsync,
    addOrRemoveWorksheetsAsync: addOrRemoveWorksheetsAsync
  };
})();
