/**
 * A module designed to build and operate a series of challenge steps, based on JSON data for the steps.
 */
GM.challenges = (function() {
  var tbl,
    json,
    currentStep = 1,
    totalSteps,
    previousTargetCellVals = {},
    mistakeCellColor = "#F3D1D2";

  // Checks the last part of the URL to see if this is a challenge page (ignoring querys and hashes)
  // Example:
  //  https://gridmaster.io/addin/courses/1/lessons/1/challenge?step=1  [challenge]  TRUE
  //  https://gridmaster.io/about                                       [about]      FALSE
  function isChallengePage() {
    return location.pathname.substr(location.pathname.lastIndexOf("/") + 1) === "challenge";
  }

  /**
   * DEFAULT ACTIONS FOR CHECKING EXERCISES.
   * We can technically override these with custom ones in the data.
   */
  /**
   * Notifies a user of progress by clearing any error status off of the status bar.
   */
  function notifyProgress(data, cellRef) {
    ExcelUtils.highlightCellsBatched(cellRef, "");
    GM.challengeMessages.printChallengeMessage("progress", data.progressMessage);
  }

  /**
   * Notifies a user of a mistake by applying an error status and error message.
   * @param {Object} data - Data, for use inside of the ErrorChecker handler.
   */
  function notifyMistake(data, cellRef) {
    ExcelUtils.highlightCellsBatched(cellRef, mistakeCellColor);
    GM.challengeMessages.printChallengeMessage("mistake", data.mistakeMessage);
  }

  /**
   * Notifies a user of completion.
   */
  function notifyCompletion(dataMessage) {
    GM.challengeMessages.printChallengeMessage("completion", dataMessage);
  }

  /**
   * Set challenge message back to its beginning state.
   */
  function resetChallengeMessage() {
    GM.challengeMessages.printChallengeMessage("starting", undefined, false);
  }

  /**
   * Sets listeners for all the steps
   */
  function setListeners() {
    var navStepEls = document.getElementsByClassName("challenge-nav-step");
    var cleanupLinks = document.querySelectorAll(".clean-up-listeners");
    var resetLinks = document.querySelectorAll(".reset-step");
    var newStep;

    for (var i = 0; i < navStepEls.length; i++) {
      navStepEls[i].addEventListener("click", function(e) {
        e.preventDefault();
        newStep = parseInt(e.currentTarget.dataset.stepNumber, 10);
        updateStep(newStep);
      });
    }

    for (var j = 0; j < cleanupLinks.length; j++) {
      cleanupLinks[j].addEventListener("click", cleanupThenGoToHref);
    }

    for (var k = 0; k < resetLinks.length; k++) {
      resetLinks[k].addEventListener("click", reset);
    }
  }

  function cleanupThenGoToHref(event) {
    var href = $(this).attr("href");

    event.preventDefault();
    clearAddinChallengeContent();
    cleanupAnyChallengeArtifacts(function() {
      location.assign(href);
    }, true);
  }

  /**
   * Resets the current challenge step.
   */
  function reset(event) {
    if (event) {
      event.preventDefault();
    }
    updateStep(currentStep);
  }

  /**
   * Refresh the page with exercise content, a new spreadsheet, and validation,
   * based on the json data and current step.
   */
  function buildStep() {
    var stepData = json.steps[currentStep - 1],
      checker,
      validationFunction;

    checker = GM.checker = new ExerciseChecker(
      {
        successAction: notifyProgress,
        failureAction: notifyMistake
      },
      {
        callback: notifyCompletion,
        data: stepData.finalMessage
      }
    );

    if (GM.checks) {
      checker.addChecksToLibrary(GM.checks);
    }

    // @todo: I may be able to clean up this code more by chaining all my promises
    //        instead of using callbacks or jumping from function to function. I'd
    //        do this by converting the deferreds to `OfficeExtension.Promise`'s
    //        return a promise from setupTargetCellsAsync, and keep the then()
    //        chain going.
    ExcelUtils.addOrRemoveWorksheetsAsync(stepData).then(function() {
      return setupTargetCellsAsync(stepData, checker);
    });
  }

  function setupTargetCellsAsync(stepData, checker) {
    var cellValidatorDeferreds = [];
    previousTargetCellVals = {};

    $.each(stepData.worksheets, function(index, worksheet) {
      $.each(worksheet.checkData, function(cellRef, cellChecks) {
        var fullCellRef = worksheet.name + "!" + cellRef;
        var validatorDeferred = new $.Deferred();
        cellValidatorDeferreds.push(validatorDeferred);

        previousTargetCellVals[fullCellRef] = ExcelUtils.getCellRefValueFromMatrix(
          cellRef,
          worksheet.tableData
        );

        checker.registerCriteria(fullCellRef, cellChecks);
        validationFunction = checker.getValidationFunction(fullCellRef);

        setupCellValidator(fullCellRef, validationFunction, validatorDeferred);
      });
    });

    $.when.apply($, cellValidatorDeferreds).done(function allValidatorsAreSet() {
      resetChallengeMessage();
      updateAddinChallengeContent();
      return ExcelUtils.updateWorksheetsAsync(stepData.worksheets);
    });
  }

  /**
   * Updates instructions, step numbers, and other add-in challenge content.
   */
  function updateAddinChallengeContent() {
    var titleEl = document.getElementById("nav-title"),
      instructionsEl = document.getElementById("instruction"),
      currentStepEl = document.querySelector(".current-step-number"),
      currentStepInCompletionForm = document.getElementById("challenge-completion--completed-step"),
      totalStepsEl = document.querySelector(".total-steps"),
      navStepEls = document.getElementsByClassName("challenge-nav-step");

    // Update text.
    titleEl.textContent = json.title;
    instructionsEl.innerHTML = json.steps[currentStep - 1].instructions;
    currentStepEl.textContent = currentStep;
    totalStepsEl.textContent = totalSteps;
    currentStepInCompletionForm.value = currentStep;

    // Replace active state
    for (var i = 0; i < navStepEls.length; i++) {
      navStepEls[i].classList.remove("is-selected");
      if (i + 1 === currentStep) {
        navStepEls[i].classList.add("is-selected");
      }
    }

    // Restore any content listeners
    var cleanupLinks = document.querySelectorAll("#instruction .clean-up-listeners");
    for (var j = 0; j < cleanupLinks.length; j++) {
      cleanupLinks[j].addEventListener("click", cleanupThenGoToHref);
    }
  }

  function clearAddinChallengeContent() {
    document.querySelector(".challenge-content").innerHTML = "";
  }

  function clearAddinInstructions() {
    document.querySelector("#instruction").innerHTML = "";
    GM.challengeMessages.removeExistingChallengeMessages();
  }

  /**
   * Sets up validation that runs on changes to the provided cell reference.
   * Abstracts away all the bindings and read/write interactions with Excel.
   *   Currently assumes:
   *     - bindings are for single cells
   *     - all binding IDs are formatted like 'Cell:${cellRef}' (ex: 'Cell:Sheet1!A2')
   * @param {String} fullCellRef          - ex. Sheet1!C5
   * @param {Function} validationFunction - An exercise-checker validation function.
   */
  // @todo: refactor me, if possible. There's lots of nesting.
  function setupCellValidator(fullCellRef, validationFunction, validatorDeferred) {
    var bindingName = "Cell:" + fullCellRef;
    var bindingTarget = ExcelUtils.quoteReferenceIfNecessary(fullCellRef);
    Office.context.document.bindings.addFromNamedItemAsync(
      bindingTarget,
      "text",
      { id: bindingName, asyncContext: validatorDeferred },
      function bindingCallback(addBindingResult) {
        if (addBindingResult.status === Office.AsyncResultStatus.Succeeded) {
          addBindingResult.value.addHandlerAsync(
            Office.EventType.BindingDataChanged,
            function handler(changeEvent) {
              validateCellAsync(fullCellRef, validationFunction);
            },
            { asyncContext: addBindingResult.asyncContext },
            function onHandlerAdded(addHandlerResult) {
              if (addHandlerResult.status === Office.AsyncResultStatus.Succeeded) {
                addHandlerResult.asyncContext.resolve();
              } else {
                console.log("The handler for " + bindingName + " wasnt added");
                console.log(addHandlerResult.error);
                addHandlerResult.asyncContext.reject();
              }
            }
          );
        } else {
          console.log("The binding for " + bindingName + " wasnt added");
          console.log(addBindingResult.error);
          addBindingResult.asyncContext.reject();
        }
      }
    );
  }

  /**
   * Runs a validation function on a cell.
   * Caveats:
   *   - Currently it assumes the cell reference is in the activeWorksheet.
   *
   * @param {String} fullCellRef - ex: 'Sheet1!C5'
   * @param {Function} validationFunction - An exercise-checker validation function.
   */
  function validateCellAsync(fullCellRef, validationFunction) {
    var cellRefParts = fullCellRef.split("!");
    var worksheetName = cellRefParts[0];
    var cellRef = cellRefParts[1];

    Excel.run(function(ctx) {
      var challengeSheet = ctx.workbook.worksheets.getItem(worksheetName);
      var cell = challengeSheet.getRange(cellRef);

      cell.load(["values", "formulas"]);
      return ctx.sync().then(function runValidationFunction() {
        var postValue = cell.values[0][0];
        var preValue = cell.formulas[0][0];

        // We are intentionally doing a loose-type check here because there are cases when
        // our tableData has numbers like "5", which then get formatted like $5.00. A
        // strict check would fail here and run the validation function, which we don't want.
        //
        // This solution doesn't fix boolean comparisons (because "true" ≠ true in javascript)
        // We handle this case by putting raw boolean values into our tableData instead of strings.
        // Maybe we should consider doing that for raw numbers too.
        //
        // @todo: we may be able to remove all this checking by refactoring to use the new events api.
        var formulaIsUnchanged = previousTargetCellVals[fullCellRef] == preValue;
        if (formulaIsUnchanged) {
          // This can happen when theres a formatting change, column/row width change,
          // excel error, or when populating the spreadsheet after the handlers are set.
          return;
        }

        previousTargetCellVals[fullCellRef] = preValue;
        validationFunction(preValue, postValue);
      });
    }).catch(ExcelUtils.logExcelRunErrors);
  }

  /**
   * Removes all currently existing bindings, BindingDataChanged handlers
   * and challenge-related cell formats from the spreadsheet.
   *
   * @param {Function} callback - Function to call when all bindings have been removed
   * @param {Boolean} removeHighlights - Optional: Determines whether to remove highlights or not.
   *                                     Removing highlights has a slight performance cost.
   */
  function cleanupAnyChallengeArtifacts(callback, removeHighlights) {
    if (!Office.context.document) {
      // There is no excel document to remove artifacts from, so we skip it.
      callback();
    }

    Office.context.document.bindings.getAllAsync(function(asyncResult) {
      if (asyncResult.status == Office.AsyncResultStatus.Failed) {
        console.log("Error: " + asyncResult.error.message);
      } else {
        var deferredArray = [];
        var refsArray = [];
        // Loop through all bindings, removing each one individually.
        // We use jQuery deferreds to run the callback function when all are done.
        // See http://stackoverflow.com/a/10004136/1154642
        $.each(asyncResult.value, function(index, binding) {
          var fullCellRef = binding.id.replace("Cell:", ""); // copies the substring. non-destructive.
          var handlerDeferred = new $.Deferred();
          var bindingDeferred = new $.Deferred();

          refsArray.push(fullCellRef);
          deferredArray.push(handlerDeferred);
          deferredArray.push(bindingDeferred);

          // We remove event handlers
          binding.removeHandlerAsync(
            Office.EventType.BindingDataChanged,
            { asyncContext: handlerDeferred },
            function onHandlerRemoved(result) {
              if (result.status == Office.AsyncResultStatus.Failed) {
                console.log("Error: " + result.error.message);
              } else {
                result.asyncContext.resolve();
              }
            }
          );

          // We remove bindings
          Office.context.document.bindings.releaseByIdAsync(
            binding.id,
            { asyncContext: bindingDeferred },
            function onBindingReleased(result) {
              if (result.status == Office.AsyncResultStatus.Failed) {
                console.log("Error: " + result.error.message);
              } else {
                result.asyncContext.resolve();
              }
            }
          );
        });

        $.when.apply($, deferredArray).done(function() {
          if (removeHighlights) {
            ExcelUtils.clearHighlights(refsArray, callback);
          } else {
            callback();
          }
        });
      }
    });
  }

  /**
   * This pulls our down our JSON challenge data and stores it as a usable Javascript object.
   * @param dataString {string} - a string of JSON containing challenge data
   */
  function parseChallengeData(dataString) {
    json = JSON.parse(dataString);

    // If the data isn't already organized into "worksheets" (like many of the earliest courses), we do it manually here.
    var stepsWithWorksheets = json.steps.map(function(step) {
      if (step.worksheets) {
        return step;
      }
      var worksheet = $.extend({ name: "Sheet1" }, step);
      delete worksheet.instructions;
      delete worksheet.finalMessage;

      return {
        instructions: step.instructions,
        worksheets: [worksheet],
        finalMessage: step.finalMessage
      };
    });

    json.steps = stepsWithWorksheets;

    // We need to convert the regex strings in our JSON to regex objects, because we cannot store regex objects natively
    // in JSON.
    $.each(json.steps, function(index, step) {
      $.each(step.worksheets, function(index, worksheet) {
        $.each(worksheet.checkData, function(cellRef, cellChecks) {
          $.each(cellChecks, function(index, checkObj) {
            if (checkObj.regex) {
              checkObj.regex = new RegExp(checkObj.regex, "i");
            }
            if (checkObj.test) {
              checkObj.test = new Function("return " + checkObj.test)();
            }
          });
        });
      });
    });
  }

  /**
   * Updates the challenge to a new step. This is called in lieu of an href going to a new page.
   *   1. changes the currentStep
   *   2. updates the URL,
   *   3. and loads the new page content.
   * @param {Number} newStepNumber - The number of the new step that we are navigating to.
   */
  function updateStep(newStepNumber) {
    if (newStepNumber > json.steps.length) {
      clearAddinChallengeContent();
      cleanupAnyChallengeArtifacts(redirectNext, true);
      return false;
    }

    currentStep = newStepNumber;
    location.hash = newStepNumber.toString();
    clearAddinInstructions();
    cleanupAnyChallengeArtifacts(buildStep);
  }

  /**
   * Uses the current path to determine the next page to direct to.
   *
   *   Before: /addin/courses/1/lessons/1/challenge#7
   *   After:  /addin/courses/1/lessons/2
   *      or   /addin/courses/1/conclusion
   *
   * Run this function upon completing the challenge.
   */
  function redirectNext() {
    var currentLessonPath = location.pathname // before: /addin/courses/1/lessons/1/challenge#7
        .substr(0, location.pathname.lastIndexOf("/")), // after:  /addin/courses/1/lessons/1
      currentLessonNum = parseInt(currentLessonPath.split("/").pop(), 10),
      nextLessonNum = (currentLessonNum += 1),
      finalLessonNum = $("data").data("lesson-total"),
      nextLessonPath;

    if (nextLessonNum > finalLessonNum) {
      // build the path to:  /addin/courses/:course_id/conclusion
      // split the url by '/', take only the beginning segments we want, and append /conclusion.
      nextLessonPath =
        location.pathname
          .split("/")
          .slice(0, 4)
          .join("/") + "/conclusion";
    } else {
      nextLessonPath =
        currentLessonPath // before: /addin/courses/1/lessons/1
          .substr(0, currentLessonPath.lastIndexOf("/") + 1) + nextLessonNum; // after:  /addin/courses/1/lessons/2
    }

    location.assign(nextLessonPath);
  }

  /**
   * Determines which step to load, based on the URL fragment. Makes corrections for any bad inputs.
   * @return {Number}     - The step number.
   */
  function determineStep() {
    var step,
      defaultStep = 1,
      numberOfSteps = json.steps.length,
      urlStep = parseInt(location.hash.substring(1));

    if (!isNaN(urlStep) && isFinite(urlStep) && urlStep <= numberOfSteps) {
      step = urlStep;
    } else {
      // The hash value wasn't good, so we need to use a fallback/
      step = currentStep || defaultStep;
      location.hash = step;
    }

    return step;
  }

  function bootstrapChallenge() {
    var dataEl = document.getElementById("inline-data");
    parseChallengeData(dataEl.innerHTML);
    currentStep = determineStep();
    totalSteps = json.steps.length;
    setListeners();
    cleanupAnyChallengeArtifacts(buildStep);
  }

  function init() {
    var dataEl = document.getElementById("inline-data");

    if (!isChallengePage() || !dataEl || !dataEl.innerHTML) {
      return;
    }

    GM.overwriteDocument.checkSpreadsheetForUserContentAsync(bootstrapChallenge);
  }

  return {
    isChallengePage: isChallengePage,
    getCurrentStep: function() {
      return currentStep;
    },
    updateStep: updateStep,
    currentStep: currentStep,
    reset: reset,
    init: init,
    bootstrapChallenge: bootstrapChallenge,
    cleanupAnyChallengeArtifacts: cleanupAnyChallengeArtifacts,
    clearAddinChallengeContent: clearAddinChallengeContent,
    clearAddinInstructions: clearAddinInstructions
  };
})();

GM.initializer.register(GM.challenges.init);
