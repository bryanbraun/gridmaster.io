/**
 * Allows javascript modules to register and run init functions
 * when Office.js is loaded and ready. Modules that only need the
 * DOM to be ready can register with $(document).ready() instead.
 */
GM.initializer = (function() {

  var initFunctions = [],
      initialized = false;

  // Registers functions to be initialized with the application after the DOM and office
  // environments are ready. If initialization has already happened, we just run the
  // function immediately.
  function register(initFunction) {
    if (initialized === false) {
      initFunctions.push(initFunction);
    } else {
      initFunction();
    }
  }

  // Runs all initialization functions registered to the app.
  function run() {
    for (var i = 0; i < initFunctions.length; i++) {
      initFunctions[i]();
    }
    initialized = true;
  };

  return {
    register: register,
    run: run
  };
})();
