// This is a manifest file that'll be compiled into application.js.
//
// This bundle will be used on all the "main", non-course pages of the website.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// To simplify things I'm going to try to avoid including manifests in other manifests. That means
// some duplication but I don't care for now, since I think it'll add clarity.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// Notes:
// - jQuery is required by rails because it is a dependency of UJS.
// - UJS adds tons of helper functionality to rails like form features, non-get requests, etc.
//   for more details, see https://github.com/rails/jquery-ujs
//
// VENDOR
//= require jquery
//= require jquery_ujs
//= require modernizr-2.8.3.min
//= require outline
//= require micromodal.min
//= require player.min
//*= require rrssb.min
//
// CUSTOM
//= require main
//= require hard-post
//= require made-with-x
//
// I've removed the = that would pull these js requirement in from gems, since I don't need them now.
////////////////////////
// require turbolinks //
////////////////////////
