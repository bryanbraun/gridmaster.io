class CityConstraint
  def matches?(request)
    City.pluck('slug').include?(request.params[:slug])
  end
end
