# By extracting progress saving into a service object, we can call it from multiple
# controllers, allowing us to save progress as part of several different workflows
# (challenge-completion, login, signup, etc).
#
# For more details about service objects, see:
# https://medium.freecodecamp.org/service-objects-explained-simply-for-ruby-on-rails-5-a8cc42a5441f
class CompletionService

  def save_progress(passed_params)
    challenge_completion_params = passed_params
    lesson_completion_params = challenge_completion_params.except(:challenge_step)
    course_completion_params = lesson_completion_params.except(:lesson_id)
    challenge_step_number = challenge_completion_params[:challenge_step].to_i

    existing_challenge_completion = ChallengeCompletion.find_by(challenge_completion_params)

    if existing_challenge_completion
      existing_challenge_completion.touch
      return {
        updated: false,
        updated_step_number: nil,
        message: 'This challenge has already been completed.'
      }
    end

    challenge_completion = ChallengeCompletion.new(challenge_completion_params)
    challenge_completion.save

    challenge_steps_completed = ChallengeCompletion.where(lesson_completion_params).distinct.count(:challenge_step)
    total_challenges_in_this_lesson = Lesson.find(challenge_completion_params[:lesson_id]).challenge_data['steps'].size

    if challenge_steps_completed == total_challenges_in_this_lesson
      # Create or update a lesson completion
      lesson_completion = LessonCompletion.find_or_initialize_by(lesson_completion_params)
      lesson_completion.updated_at = Time.now
      lesson_completion.save

      # Check if all lessons are completed
      lessons_completed_count = LessonCompletion.where(course_completion_params).distinct.count(:lesson_id)
      total_lessons_in_this_course = Course.find(challenge_completion_params[:course_id]).lessons.size

      if lessons_completed_count == total_lessons_in_this_course
        # Create or update a course completion
        course_completion = CourseCompletion.find_or_initialize_by(course_completion_params)
        course_completion.updated_at = Time.now
        course_completion.save
      end
    end

    return {
      updated: true,
      updated_step_number: challenge_step_number,
      message: 'This challenge completion is now recorded.'
    }
  end

end
