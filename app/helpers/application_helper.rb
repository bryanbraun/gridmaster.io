require 'uri'

module ApplicationHelper

  def full_title(page_title = '')
    if page_title.empty?
      "Gridmaster: Learn Excel by Doing"
    else
      page_title + " | Gridmaster"
    end
  end

  def body_class
    "#{controller.controller_name}-#{controller.action_name}"
  end

  def last_segment_of_current_url(url = request.url)
    URI(url).path.split("/").last
  end

  def url_a_level_above_current_url(url = request.url)
    File.dirname(url)
  end

  def addin_page?(url = request.url)
    path_segments = URI(url).path.split("/").delete_if(&:empty?)
    return path_segments.first == 'addin'
  end

  # Customize default classes for flash messages to match the ones in Office Fabric
  #   Fabric Message Bar Docs: https://github.com/OfficeDev/office-ui-fabric-js/blob/4697cb1f164d0a027d2bb6af6671d486bb438493/ghdocs/components/MessageBar.md
  #   Technique: https://stackoverflow.com/a/9594123/1154642
  def addin_flash_class(level)
    case level.to_sym
      when :notice then "ms-MessageBar--info"
      when :success then "ms-MessageBar--success"
      when :error then "ms-MessageBar--error"
      when :recaptcha_error then "ms-MessageBar--error"
      when :alert then "ms-MessageBar--warning"
    end
  end
  def addin_flash_icon(level)
    case level.to_sym
      when :notice then "ms-Icon--Info"
      when :success then "ms-Icon--Completed"
      when :error then "ms-Icon--ErrorBadge"
      when :recaptcha_error then "ms-Icon--ErrorBadge"
      when :alert then "ms-Icon--Info"
    end
  end
end
