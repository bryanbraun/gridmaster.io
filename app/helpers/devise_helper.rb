module DeviseHelper
  # Overridden from https://github.com/plataformatec/devise/blob/71fc5b351a8e8473733b0ca29d60c06e43a0efe1/app/helpers/devise_helper.rb
  # To customize markup.
  def devise_error_messages!
    return "" if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
                      count: resource.errors.count,
                      resource: resource.class.model_name.human.downcase)

    if addin_page?
      html = <<-HTML
      <div class="ms-MessageBar ms-MessageBar--error devise-validation">
        <div class="ms-MessageBar-content">
          <div class="ms-MessageBar-icon">
            <i class="ms-Icon ms-Icon--ErrorBadge"></i>
          </div>

          <div class="ms-MessageBar-text">
            <div><strong>#{sentence}</strong></div>
            <ul>#{messages}</ul>
          </div>
        </div>
      </div>
      HTML
    else
      html = <<-HTML
      <div class="message-box warning devise-validation">
        <div>#{sentence}</div>
        <ul>#{messages}</ul>
      </div>
      HTML
    end

    html.html_safe
  end
end
