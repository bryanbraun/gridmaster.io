source 'https://rubygems.org'

# READ THIS, IF ADDING NEW GEMS:
#
# For successful docker deploys, add the gem below, run "bundle install" locally
# (to update the Gemfile.lock), and THEN build the containers before starting them.
# This will result in containers with all the expected dependencies.

gem 'dotenv-rails'

# Upgrade note: when upgrading to 5.1, make this change: https://stackoverflow.com/a/48668905/1154642
gem 'rails', '5.0.2'

gem 'pg', '~> 0.18'
gem 'puma', '~> 3.10.0'
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', '~> 0.12.3', platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# I'm considering this an optimization for the future.
# gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.11'
gem 'administrate', '~>0.7.0'
gem 'administrate-field-json', :git => 'https://github.com/eddietejeda/administrate-field-json.git', :ref => '7d1ef26'
gem 'devise', '~>4.4.0'
gem 'mail_form', '~> 1.7.0'
gem 'stripe', '~> 3.5.2'
gem "recaptcha", require: "recaptcha/rails"
gem 'sitemap_generator', '~>6.0.1'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'sshkit', '~>1.13.1'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.8'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'rails-controller-testing', '0.1.1'
  gem 'minitest-reporters',       '1.1.9'
  gem 'guard',                    '2.13.0' # todo, learn how to use this.
  gem 'guard-minitest',           '2.4.4'
  gem 'stripe-ruby-mock',         '~> 2.5.0', :require => 'stripe_mock'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
