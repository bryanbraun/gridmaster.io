# Based on the deploy script at http://chrisstump.online/2016/03/17/continuous-deployment-docker-rails/
require 'sshkit'
require 'sshkit/dsl'

include SSHKit::DSL

# set the identifier used to used to tag our Docker images
deploy_tag = ENV['DEPLOY_TAG']
rails_image = ENV['RAILS_IMAGE'] || "registry.gitlab.com/bryanbraun/gridmaster.io/railsapp:#{ENV['DEPLOY_TAG']}"
nginx_image = ENV['NGINX_IMAGE'] || "registry.gitlab.com/bryanbraun/gridmaster.io/nginx:#{ENV['DEPLOY_TAG']}"

# set the name of the environment we are deploying to (e.g. staging, production, etc.)
deploy_env = ENV['DEPLOY_ENV'] || :production

# set the location on the server of where we want files copied to and commands executed from
deploy_path = ENV['DEPLOY_PATH'] || "/home/#{ENV['SERVER_USER']}"

# assign the token we'll use to login to the gitlab registry
# (use REGISTRY_USER & REGISTRY_PASS when deploying locally and CI_JOB_TOKEN when deploying from CI)
registry_user = ENV['REGISTRY_USER'] || "gitlab-ci-token"
registry_pass = ENV['REGISTRY_PASS'] || ENV['CI_JOB_TOKEN']

# connect to server
server = SSHKit::Host.new hostname: ENV['SERVER_HOST'], user: ENV['SERVER_USER']

namespace :deploy do
  desc 'copy to server files needed to run and manage Docker containers'
  task :configs do
    on server do
      upload! File.expand_path('../../config/containers/docker-compose.production.yml', __dir__), deploy_path
    end
  end
end

namespace :sitemap do
  desc 'Run sitemap:refresh inside of our production rails container'
  task :refresh_in_prod do
    on server do
      within deploy_path do
        rails_container_id = capture("docker-compose", "-f", "docker-compose.production.yml", "ps", "-q", "app")
        execute "docker", "exec", rails_container_id, "rails", "sitemap:refresh"
      end
    end
  end
end

namespace :docker do
  desc 'logs into gitlab container registry for pushing and pulling'
  task :login do
    on server do
      within deploy_path do
        execute 'docker', 'login', '-u', registry_user, '-p', registry_pass, 'https://registry.gitlab.com'
      end
    end
  end

  desc 'stops all Docker containers via Docker Compose'
  task stop: 'deploy:configs' do
    on server do
      within deploy_path do
        with rails_env: deploy_env, deploy_tag: deploy_tag do
          execute 'docker-compose', '-f', 'docker-compose.production.yml', 'stop'
        end
      end
    end
  end

  desc 'starts all Docker containers via Docker Compose'
  task start: 'deploy:configs' do
    on server do
      within deploy_path do
        with rails_env: deploy_env, deploy_tag: deploy_tag do
          execute 'docker-compose', '-f', 'docker-compose.production.yml', 'up', '-d'

          # write the deploy tag to file so we can easily identify the running build
          execute 'echo', deploy_tag , '>', 'deploy.tag'
        end
      end
    end
  end

  desc 'pulls images from Docker Hub'
  task pull: 'docker:login' do
    on server do
      within deploy_path do
        execute 'docker', 'pull', nginx_image
        execute 'docker', 'pull', rails_image
        execute 'docker', 'pull', 'postgres:9.6.1'
      end
    end
  end

  desc 'runs database migrations in application container via Docker Compose'
  task migrate: 'deploy:configs' do
    on server do
      within deploy_path do
        with rails_env: deploy_env, deploy_tag: deploy_tag do
          execute 'docker-compose', '-f', 'docker-compose.production.yml', 'run', '--rm', 'app', 'bundle', 'exec', 'rails', 'db:migrate'
        end
      end
    end
  end

  desc 'pulls images, stops old containers, updates the database, and starts new containers'

  # Combine all tasks into a single deploy task. Items of note:
  # 1. We pull images manually (instead of part of `docker-compose up`) to reduce down time.
  task deploy: %w{docker:pull docker:stop docker:migrate docker:start sitemap:refresh_in_prod}
end
