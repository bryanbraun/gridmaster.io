desc "Populate cities in the database from the cities.txt file"
task :populate_cities => :environment do
  puts "Deleting all existing cities."
  City.delete_all

  cities = YAML.load_file("#{Rails.root}/lib/assets/cities.txt")
  cities.each do |city_name|
    City.create!(name: city_name, slug: city_name.parameterize)
  end
  puts "All new cities have been populated."
end
