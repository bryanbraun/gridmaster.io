# For details on why we have this task, see:
# http://yerb.net/blog/2014/03/13/three-easy-steps-to-using-counter-caches-in-rails/

desc "Manually reset the cache counters in my database tables"
task :reset_cache_counters => :environment do
  # This currently only applies to the lessons_count in my "courses" table.
  Course.reset_column_information
  Course.ids.each do |id|
    Course.reset_counters(id, :lessons)
  end
  puts "The cache counters have been reset."
end
