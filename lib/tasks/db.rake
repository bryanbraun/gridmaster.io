# additional tasks for general database management.
require 'sshkit'
require 'sshkit/dsl'

include SSHKit::DSL

namespace :db do
  # If database writes (through administrate, for example) are failing with errors like...
  #
  #   `ActiveRecord::RecordNotUnique (PG::UniqueViolation: ERROR:  duplicate key value violates unique constraint`
  #
  # ...this task should adjust DB settings so it's no longer an issue. For details, see:
  # https://stackoverflow.com/a/15108735/1154642
  desc "Fixes the issue of Postgres assigning to existing keys"
  task fix_pk_sequence: :environment do
    ActiveRecord::Base.connection.tables.each do |t|
      ActiveRecord::Base.connection.reset_pk_sequence!(t)
    end
  end

  desc "Make a prod database backup and save to the prod server and your local computer"
  task :backup do
    server = SSHKit::Host.new hostname: ENV['SERVER_HOST'], user: ENV['SERVER_USER']
    backup_location = "/root/backups"
    filename = "prod-backup__#{Time.now.to_f}.sql"
    download_location = Rails.root.join("db", "backups", filename)

    on server do
      within backup_location do
        pg_container_id = capture("docker-compose", "-f", "../docker-compose.production.yml", "ps", "-q", "postgres")
        execute "docker", "exec", pg_container_id, "pg_dump", "-d", "gridmaster_prod", "-U", "postgres", "-E", "utf8", ">", filename
      end
    end

    # We're running the scp shell command because sshkit's download! was having issues with text encoding in the db files.
    sh "scp #{ENV['SERVER_USER']}@#{ENV['SERVER_HOST']}:#{backup_location}/#{filename} #{download_location}"
    puts "#{filename} was saved on the server in #{backup_location} and downloaded to #{download_location}"
  end
end
