require 'fileutils'

desc "Runs the VorlonJS server"
task :vorlon do
  puts "Starting up the VorlonJS debugging tool"
  puts "If https doesn't work, remember to update node_modules/vorlon/Server/config.json"
  puts "(see http://vorlonjs.com/documentation/#ssl-support)"
  sh %{node node_modules/vorlon/bin/vorlon} do |ok, res|
    if ! ok
      puts "Could not run VorlonJS. Maybe try 'npm install'?"
    end
  end
end
