# In this folder

This folder contains JSON files for Addin challenges.

These specific files are not used by the site. They are simply backups of the JSON data is stored in the database for each challenge.

We check these into version control for a few reasons:

1. To make it easier to communicate around the challenges and propose changes in code.
2. So it's easy to restore challenge data specifically (because since a misplaced comma in the admin panel can blow away the whole JSON structure in production)

We tend to create the lessons in JSON files anyways, so we might as well keep them around.
