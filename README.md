# gridmaster.io

Build: [![build status](https://gitlab.com/bryanbraun/gridmaster.io/badges/master/build.svg)](https://gitlab.com/bryanbraun/gridmaster.io/commits/master)

Ruby Coverage: [![coverage report](https://gitlab.com/bryanbraun/gridmaster.io/badges/master/coverage.svg)](https://gitlab.com/bryanbraun/gridmaster.io/commits/master)

## Getting Started

To run this application locally.

1. Pull this repo down to your computer.
2. Duplicate `.env.example`, rename it to `.env`, and populate the local environment variables with their correct values.
3. Install docker (and docker-compose).
4. From the project root, run `docker-compose up -d`

It's useful to have a shell open in the running Rails application container. To get this, run:

```
docker exec -it gridmaster_app_1 /bin/bash
```

From this terminal you can:

* Run all tests: `rails test`
* Run a single test (eg. on line 11): `rails test test/models/tip_test.rb:11`
* Run migrations: `rails db:migrate`
* Generate new files: `rails generate migration migrate_to_vimeo`
* Explore your data: `rails console`
* Find and run other tasks: `rails`

**Debugging**

1. Attach a terminal session to the rails container running your application:

   docker attach gridmaster_app_1

2. Insert `byebug` into the code where you want to pause execution.
3. Load the page, then check your terminal session where you'll see the debugger waiting for you.

## Host Server setup

* Ensure it has docker and docker-compose installed
* Create a `.env` file defining things like SECRET_KEY_BASE, POSTGRES_PASSWORD, and RAILS_ENV.
* Put TLS certs / keys into a certs folder in the project root.

## High-level overview of technologies and approaches

Gridmaster is a single Rails codebase and Postgres database, powering a few applications, including:

**The Gridmaster.io Website**

Path: `https://gridmaster.io`

**The Excel Addin**

Path `https://gridmaster.io/addin`

**The Admin interface**

Path `https://gridmaster.io/admin`

Currently, we use basic auth to
access the admin interface (which is powered by [Administrate](https://github.com/thoughtbot/administrate))

### Authentication / Authorization:

We are using Devise for authentication / authorization, for both the addin and website. Both apps share the same User model (and database tables), but they each can have their own controllers and views.

## Deploying

To deploy code:

1. Push up to gitlab: `git push gitlab master`
2. Once the build passes, either:
   1. Faster: Deploy from local machine with: `DEPLOY_TAG=[--most recent git sha--] rails docker:deploy`
   2. Slower: Deploy from the gitlab UI by clicking the "Play" button next to the most recent pipeline

@todo: insert an architecture diagram here that shows how deploys work by building tested docker images, storing them in a registry, and having prod pull them down and run them.

## Backups

To create a database backup run this task manually:

```
rails db:backup
```

This will create a database backup, save it to filesystem on the prod server, and then download a copy to your `/db/backups` folder (which should be gitignored). If you are restricted from doing this, you may need to add your SSH key to Digital Ocean.
