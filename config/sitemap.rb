# A sitemap is just an XML file. We could build it with a standard rails view,
# but that has performance issues if there are lots of links (see:
# https://github.com/kjvarga/sitemap_generator/issues/190#issuecomment-101789705).
#
# Thus, we generate a static version using the popular sitemap_generator gem. In order
# to automate this process as much as possible, we run the generate task in production
# after the site is deployed (because we need access to production data, like "tips").
# Unfortunately, some of our links get created outside of the deploy process (like tips
# created via the Adminstrate panel), but as long as we have semi-regular deploys things
# shouldn't get too out of date.

require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'https://gridmaster.io'
SitemapGenerator::Sitemap.create do
  # Basic pages routes
  add about_path
  add addin_support_path
  add privacy_path
  add pricing_path
  add join_tips_path
  add libraries_path
  add community_centers_path
  add training_path
  # add purchase_complete_path # We probably don't need to link our confirmation page.

  # Public "users" routes
  add new_user_session_path
  add new_user_password_path
  add new_user_registration_path
  add new_user_confirmation_path

  # Tips
  add tips_path, changefreq: 'weekly'
  Tip.where({ published: true }).find_each do |tip|
    add tip_path(slug: tip.slug), changefreq: 'monthly'
  end

  # Cities
  City.distinct.pluck(:slug).each do |slug|
    add city_path(slug: slug), changefreq: 'monthly'
  end

end
