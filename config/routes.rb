

Rails.application.routes.draw do

  root   'basic_pages#home'
  get    '/about',                to: 'basic_pages#about'
  get    '/addin-support',        to: 'basic_pages#addin_support'
  get    '/privacy',              to: 'basic_pages#privacy'
  get    '/pricing',              to: 'basic_pages#pricing'
  get    '/purchase-complete',    to: 'basic_pages#purchase_complete'
  get    '/join-tips',            to: 'basic_pages#join_tips'
  get    '/libraries',            to: 'basic_pages#libraries'
  get    '/community-centers',    to: 'basic_pages#community_centers'
  get    '/training',             to: 'basic_pages#training'
  get    '/excel-training/:slug', to: 'basic_pages#cities', as: 'city', constraints: CityConstraint.new

  resources :tips, path: "spreadsheet-tips", param: :slug, only: [:index, :show]
  get '/tips', to: redirect('/spreadsheet-tips')
  get '/tips/:tip_name', to: redirect('/spreadsheet-tips/%{tip_name}')

  resources :charges, only: [:create]

  # Devise routes for the main website.
  devise_for :users,
    controllers: {
      passwords: 'users/passwords',
      confirmations: 'users/confirmations',
      registrations: 'users/registrations',
      sessions: 'users/sessions'
    },
    path_names: {
      sign_in: 'login',
      sign_out: 'logout'
    }
  # Additional routes for custom actions
  devise_scope :user do
    put 'users/update_password', to: 'users/registrations#update_password'
    put 'users/update_info', to: 'users/registrations#update_info'
  end
  # Short url with redirect created for giveaway cards and other printed materials.
  get '/sign-up', to: redirect('/users/sign_up')

  # Redirects from version 1 of the product (since there are still some old links out there)
  get '/courses(/*all)', to: redirect(path: '/#courses')

  # Routes namespaced under /addin
  namespace :addin do
    root 'basic_pages#index', as: :root
    get  'purchase-complete', to: 'basic_pages#purchase_complete'

    resources :charges, only: [:create]

    # Creates
    #  /addin/courses
    resources :courses, only: [:index, :show] do

      # Creates
      #   /addin/courses/:id/conclusion
      get 'conclusion', on: :member

      # Creates
      #  /addin/courses/:id/lessons
      #  /addin/courses/:id/lessons/:number_in_course
      resources :lessons, param: :number_in_course, only: [:index, :show] do

        # Creates
        #   /addin/courses/:id/lessons/:number_in_course/challenge
        #   /addin/courses/:id/lessons/:number_in_course/full-screen-video
        get 'challenge', on: :member
        get 'fullscreen', on: :member
      end
    end

    post 'feedback/create', to: 'feedback#create'
    post 'feedback/create-challenge', to: 'feedback#createChallenge'
    post 'completions/create', to: 'completions#create'

    # Devise routes & controllers for the addin.
    #
    # The main website and addin have their own devise routes and controllers but they share the model.
    #
    # See the routes "devise_for" generates at:
    #   http://www.rubydoc.info/github/plataformatec/devise/master/ActionDispatch/Routing/Mapper%3Adevise_for
    devise_for :users, skip: [:passwords, :confirmations], skip_helpers: true,
      controllers: {
        registrations: 'addin/users/registrations',
        sessions: 'addin/users/sessions'
      },
      path_names: {
        sign_in: 'login',
        sign_out: 'logout'
      }
    # Additional routes for custom actions
    devise_scope :addin_user do
      put 'users/update_password', to: 'users/registrations#update_password'
      put 'users/update_info', to: 'users/registrations#update_info'
      get 'users/password/new', to: 'users/passwords#new'
      get 'users/confirmation/new', to: 'users/confirmations#new'
    end
    # Redirects for people using the addin's "up one level" button.
    get 'users', to: redirect('/addin/courses', status: 302)
    get 'users/password', to: redirect('/addin/users/login', status: 302)
    get 'users/confirmation', to: redirect('/addin/users/login', status: 302)
  end

  namespace :admin do
    root to: "courses#index"
    resources :courses
    resources :lessons
    resources :users
    resources :tips
    resources :course_completions
    resources :lesson_completions
    resources :challenge_completions
    resources :whitelisted_ips
  end
end
# (For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html)

