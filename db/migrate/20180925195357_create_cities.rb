class CreateCities < ActiveRecord::Migration[5.0]
  def up
    create_table :cities do |t|
      t.string :name
      t.string :slug
    end

    # Load cities into the database from file
    cities = YAML.load_file("#{Rails.root}/lib/assets/cities.txt")
    cities.each do |city_name|
      City.create!(name: city_name, slug: city_name.parameterize)
    end
  end

  def down
    drop_table :cities
  end
end
