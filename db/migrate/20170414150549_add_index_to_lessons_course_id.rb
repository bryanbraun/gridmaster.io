class AddIndexToLessonsCourseId < ActiveRecord::Migration[5.0]
  def change
    add_index :lessons, :course_id
  end
end
