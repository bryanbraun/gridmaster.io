class CreateWhitelistedIps < ActiveRecord::Migration[5.0]
  def change
    create_table :whitelisted_ips do |t|
      t.inet :ip
      t.string :comments
      t.timestamps
    end
  end
end
