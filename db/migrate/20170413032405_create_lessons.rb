class CreateLessons < ActiveRecord::Migration[5.0]
  def change
    create_table :lessons do |t|
      t.string :name
      t.integer :course_id
      t.integer :order_in_course
      t.string :youtube_video_id
      t.jsonb :challenge_data

      t.timestamps
    end
  end
end
