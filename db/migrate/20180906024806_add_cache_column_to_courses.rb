class AddCacheColumnToCourses < ActiveRecord::Migration[5.0]
  def up
    add_column :courses, :lessons_count, :integer, default: 0
    Rake::Task['reset_cache_counters'].invoke
  end

  def down
    remove_column :courses, :lessons_count, :integer
  end
end
