class MigrateToVimeo < ActiveRecord::Migration[5.0]
  def up
    rename_column :lessons, :youtube_video_id, :video_id
    add_column :courses, :aspect_ratio, :string, default: "4:3"

    add_aspect_ratios
    update_video_ids(:video_id, vimeo_ids)
  end

  def down
    rename_column :lessons, :video_id, :youtube_video_id
    remove_column :courses, :aspect_ratio, :string

    update_video_ids(:youtube_video_id, youtube_ids)
  end

  def youtube_ids
    [
      "xPdxovxS3BM",
      "6GuqMdL2mbg",
      "hx3-SJJDHTI",
      "3Q1jPBQbpI4",
      "ytrLpTegfbE",
      "ASZo_Y4Zruk",
      "cuF7b6Ukwjk",
      "WNObHaUARIA",
      "XadDqk3FXDE",
      "QSlb-Wxfsg8",
      "NezPYgAQ6Qk",
      "mSzxapaDg1w",
      "HAsoZVMNFDk",
      "f7d5lh-DU78",
      "i0CU5oeWgdU",
      "iYNiorVvi3g",
      "LzjcnS2CbE8",
      "JMdoDAi5qWg",
      "Qm87p_FVscs",
      "lVNuVwbDUy4",
      "qQUqk2ybPkQ",
      "aodkxw0hQ1g",
      "a4UgeuwIYyg",
      "8JxX8eyc_TU",
      "DykaG7E-UpA",
      "51DhdFHLSxY",
      "nOKrFBjYntI",
      "Z-RTpHRnXGw",
      "tWd1MJLDlpI",
      "nz7atrNG6ow",
      "CcYee_cMCT8",
      "QMMfolKJGPY",
      "RoDFvMMf7uc",
      "n7-O7PemQmo",
      "jEjIy6ZsTc4",
      "QzsGzrqPfMU",
      "Mv8N9D6lVwM",
    ]
  end

  def vimeo_ids
    [
      '297114083',
      '297114271',
      '297114332',
      '297114615',
      '297114466',
      '297176579',
      '297176619',
      '297176653',
      '297176675',
      '297176689',
      '297176709',
      '297176717',
      '297176727',
      '297152361',
      '297152428',
      '297152486',
      '297152600',
      '297152774',
      '297152832',
      '297151131',
      '297151417',
      '297151632',
      '297151748',
      '297151843',
      '297151940',
      '297152131',
      '297152256',
      '297166724',
      '297166823',
      '297166874',
      '297166946',
      '297166986',
      '297167018',
      '297167055',
      '297167101',
      '297167136',
      '297167176',
    ]
  end

  def update_video_ids(column_name, video_ids)
    # The index of each video_id in the array (plus 1)
    # represents the record ID of the corresponding lesson.
    video_ids.each_with_index do |video_id, index|
      Lesson.find(index + 1).update({column_name => video_id})
    end
  end

  def add_aspect_ratios
    # The index of each aspect_ratios in the array (plus 1)
    # represents the record ID of the corresponding course.
    aspect_ratios = [
      '4:3',
      '8:5',
      '4:3',
      '4:3',
      '8:5',
    ]
    aspect_ratios.each_with_index do |ratio, index|
      Course.find(index + 1).update({:aspect_ratio => ratio})
    end
  end
end
