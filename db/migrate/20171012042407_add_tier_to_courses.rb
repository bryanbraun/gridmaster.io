class AddTierToCourses < ActiveRecord::Migration[5.0]
  def change
    add_column :courses, :tier, :integer
  end
end
