class CreateCompletions < ActiveRecord::Migration[5.0]
  def change
    # An entry in any of these tables means that thing was completed.
    create_table :course_completions do |t|
      t.integer :user_id
      t.integer :course_id
      t.timestamps
    end

    create_table :lesson_completions do |t|
      t.integer :user_id
      t.integer :course_id
      t.integer :lesson_id
      t.timestamps
    end

    create_table :challenge_completions do |t|
      t.integer :user_id
      t.integer :course_id
      t.integer :lesson_id
      t.integer :challenge_step
      t.timestamps
    end
  end
end
