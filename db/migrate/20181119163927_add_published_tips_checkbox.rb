class AddPublishedTipsCheckbox < ActiveRecord::Migration[5.0]

  def up
    add_column :tips, :published, :boolean, default: false
    add_column :tips, :published_at, :datetime

    Tip.update_all(published: true)

    Tip.all.each do |tip|
      tip.published_at = tip.created_at
      tip.save
    end
  end

  def down
    remove_column :tips, :published, :boolean
    remove_column :tips, :published_at, :datetime
  end

end
