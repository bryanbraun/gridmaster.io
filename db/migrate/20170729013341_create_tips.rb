class CreateTips < ActiveRecord::Migration[5.0]
  def change
    create_table :tips do |t|
      t.string :title
      t.string :slug
      t.string :image_url
      t.string :image_alt_text
      t.text :description

      t.timestamps
    end
  end
end
